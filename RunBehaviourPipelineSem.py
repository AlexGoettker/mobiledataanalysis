# # Labelling tool for mobile eye tracking data
# 
# This code will read the raw data from the Pupil server and will give you different outputs that can be flexibly managed: 
# 
# 1. **Saccades** (based on a adapted version of [Niehorster et al., 2020](https://link.springer.com/article/10.3758/s13428-019-01314-1))
# 2. **Smooth eye movements** (pursuit and VOR)
# 3. **Fixations** (as the absence of a detected movement)
# 3. **Head Movements** 
# 4. **Automatic Scene Labelling** with Detectron2 [(Wu et al., 2019)](https://github.com/facebookresearch/detectron2)
# 
# With this you can analyze basic eye and head movement characteristics (Frequencies, amplitudes, Fixation Durations, ...) <br> as well as a good approximation what people where fixating in the real world.
# 

# ## Import relevant packages

# Remove warnings
import warnings
warnings.filterwarnings("ignore")

# Import packages
import os
import pandas as pd
import numpy as np
import glob
import cv2
#import ffmpegcv 
from scipy import signal
from sys import platform
from itertools import compress 
from itertools import count

# Plotting thigs
import matplotlib.pyplot as plt
import matplotlib.patches as pltrec
from matplotlib.widgets import Slider, Button, CheckButtons
import ipympl

# Import the analysis functions 
import AnalysisTools as at

# Setup Detectron
import torch
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger() # Run the setup logger 
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
SystemName  = platform

##########################################################################
## Import custom helper functions & setting paths 
abspath = os.path.abspath('RunBehaviourPipelineSem.py') # Get the Path of this script
dname = os.path.dirname(abspath) # Get the directory
os.chdir(dname) # Set this to your current workingdirectoy


# ## Define some global parameters of the analysis
# 
# Here we will set some thresholds etc that are then used in the following analysis
# 

global SamplingRateEye,SamplingRateHead,WindowLength,Threshold,MaxCounter,MinFix,Lambda, UpperThreshold,MinThreshold,MinDurationPursuit,MinVelocityPrctile,MinVelocityPursuit,MinDurationHead,MinVelocityPrctileHead,MinVelocityHead,WindowLengthHead,DirectionWindow,DistanceWindow,IntegrationRadius

# Sampling Rates
SamplingRateEye = 200 # Sampling rate of the eye tracking Data data in [Hz]
SamplingRateHead= 110 # Sampling rate of the eye tracking Data data in [Hz]

# For the saccade detection
WindowLength = 1000 # Length of the window you search for fixations [ms]
Threshold = 200 # Initial Threshold velocity to start searching 
MaxCounter = 200 # Number of iterations for outlier search 
MinFix = 80 # Minimum Duration for Fixation [ms]
Lambda = 2.5 # Std Criterion for outlier search 
UpperThreshold = 1200 # Upper Velocity Threshold
MinThreshold = 50 # Lower Velocity Threshold
MissThreshold = 100 # Lower Velocity Threshold

# For smooth movement detection 
MinDurationPursuit = 100 # Minimum Duration of smooth segment
MinVelocityPrctile = 68 # Minimum Velocity of Pursuit 
MinVelocityPursuit = 10 # Minimum Velocity of Pursuit 

# For head movement detection 
MinDurationHead = 100 # Minimum Duration of segment for head movement
MinVelocityPrctileHead= 68 # Minimum Velocity of Head movement within window 
MinVelocityHead = 10 # Minimum Velocity of Head 
WindowLengthHead = 2000 # Length of the window you search for fixations [ms]

# For VOR 
DirectionWindow = 30 # The direction of the eye and head movement need to be in 180 +/- the Window [°] 
DistanceWindow = 0.5 # The ratio of the eye and head distance coverd in the respective time needs to be 1- Distance Window (e.g. if 0.5 it needs to be between 50 and 150%)

# For scene labelling
IntegrationRadius =30 # Pixel radius that is considered to be fixated



# ## Now load the data
# 
# Data Structure should be as follows. Per Recording, there should be one folder that has the following files: 
# 1. gaze.csv 
# 2. imu.csv
# 3. world_timestamps.csv
# 4. world.mp4 (This file is typically named differently, for ease of use it should be renamed to world)
# 
# 

# Define the datapath to the data
Datapath = '../Data_Sem/' # Data should be a folder on the same level as the Code Folder

### Set the Name of the recording you want to analyze 

#SubjectList = ['Rec1','Rec2','Rec3','Rec4','Rec5','Rec6','Rec7','Rec8']
SubjectList = ['Rec2','Rec3','Rec4','Rec5','Rec6','Rec7','Rec8']

# SubjectList = ['Subj15']
# CatList = ['Images','UB']

for sub in SubjectList: 
        Rec = sub
        print('This is recording ', Rec)

        ######################################################
        # Get the behavioural data 
        Eye = pd.read_csv(os.path.join(Datapath,Rec,'gaze.csv')) # Get the gaze position
        Head = pd.read_csv(os.path.join(Datapath,Rec,'imu.csv')) # Get the Head position via the imu data
        Video = pd.read_csv(os.path.join(Datapath,Rec,'world_timestamps.csv')) # Get the timestamps of the video 
        LabelList = pd.read_json(os.path.join(Datapath,'panoptic_coco_categories.json')) # Read the list of the labels for Image Segmentation

        vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
        cap = cv2.VideoCapture(vidname[0]) # World Video


        ########################################################################################
        ## Add toggles to decide what to analyze 
        AnalyzeSacc = 1 # if 1 you will label saccades 
        AnalyzeSmooth = 1 # if 1 you will label smooth eye movements
        AnalyzeHead = 1 # if 1 you will analyze head movements
        AnalyzeBlinks =1 # If you want to analyze blinks
        IntFigureLabel = 1 # if 1 you will show the labeling figure 
        AnalyzeSceneLabelling = 0 # if 1 you want to analyze the scene camera
        Visualize = 0 # if 1 you will try to visualize the results 

        # #### Run some tests to check the data recordings 

        at.test_dataformats(Eye,Head,Video,cap) # test whether the shape of the data is correct

        # ## Read out the variables you need for the analysis and compute velocities
        # 
        # Things you want to have: 
        # 1. Gaze position in pixels and degrees and their sampling 
        # 2. The head position in deg 
        # 
        # Then compute the eye and head velocity (this is already available for the head movements).
        # For computing the eye velocity, you want to add a filter to the gaze data to reduce noise.
        # Also get the sampling of the different components and align them on the same time scale.
        # 
        # 

        """ This is for eye movements """

        ## Get the relevant Data 
        xpos = Eye['gaze x [px]'].to_numpy() # Eye position in pixel x 
        ypos = Eye['gaze y [px]'].to_numpy() # Eye position in pixel y 
        xdeg = Eye['azimuth [deg]'].to_numpy() # Eye position in pixel x 
        ydeg = Eye['elevation [deg]'].to_numpy() # Eye position in pixel y 
        time_eye= Eye['timestamp [ns]'].to_numpy()/10**6 # Timestamps of the eye recording divided by 10^6 to turn it into ms

        ## Setup the Filter 
        b,a = signal.butter(2, 20, 'low', fs=200, output='ba')

        ## Filter the eye data
        xpos = signal.filtfilt(b,a,xpos) # Xpos in Pixel
        ypos = signal.filtfilt(b,a,ypos) # Ypos in Pixel
        xdeg = signal.filtfilt(b,a,xdeg) # Xpos in Deg
        ydeg = signal.filtfilt(b,a,ydeg) # Ypos in Deg

        ## Compute the velocity 
        # Velocity in deg per second
        vel_x = at.HesselsVelocity(xdeg,time_eye)*1000 # Mulitply it by 1000 to make it deg/s
        vel_y = at.HesselsVelocity(ydeg,time_eye)*1000 # Mulitply it by 1000 to make it deg/s
        vel_eye = np.hypot(vel_x,vel_y)
        #vel_eye = signal.filtfilt(b,a,vel_eye) # Xpos in Pixel

        # Velocity in Pixel per second
        vel_x_pix = at.HesselsVelocity(xpos,time_eye)*1000 # Mulitply it by 1000 to make it deg/s
        vel_y_pix = at.HesselsVelocity(ypos,time_eye)*1000 # Mulitply it by 1000 to make it deg/s
        vel_eye_pix = np.hypot(vel_x_pix,vel_y_pix)


        ## Correct for Blinks, in the raw file from Pupil Labls these are interpolated and existent
        xpos[np.where(Eye['blink id'].notnull())] = np.nan
        ypos[np.where(Eye['blink id'].notnull())] = np.nan
        xdeg[np.where(Eye['blink id'].notnull())] = np.nan
        ydeg[np.where(Eye['blink id'].notnull())] = np.nan
        vel_eye[np.where(Eye['blink id'].notnull())] = np.nan
        Blink_Vek = (Eye['blink id'].notnull())


        """ This is for head movements """

        PitchDeg = Head['pitch [deg]'].to_numpy() # Read out the positions in deg
        YawDeg = Head['yaw [deg]'].to_numpy()
        RollDeg = Head['roll [deg]'].to_numpy()
        time_head = Head['timestamp [ns]'].to_numpy()/10**6

        vx_head = Head['gyro z [deg/s]'].to_numpy() *-1 # The velocities are already available from the gyroscope
        vy_head = Head['gyro x [deg/s]'].to_numpy()
        vz_head = Head['gyro y [deg/s]'].to_numpy()

        # To align the data with the video coordinate system you need to rotate the imu data by 102° around the x-axis 
        # see https://docs.pupil-labs.com/neon/data-collection/data-streams/ for details 

        theta = 102 
        #vx_head = vx_head
        #vy_head = vy_head*np.cos(np.radians(theta)) - vz_head*np.sin(np.radians(theta))
        #vz_head = vy_head*np.sin(np.radians(theta)) + vz_head*np.cos(np.radians(theta))

        #vy_head = vy_head
        #vz_head = vx_head*np.cos(np.radians(theta)) - vz_head*np.sin(np.radians(theta))
        #vx_head = vx_head*np.sin(np.radians(theta)) + vz_head*np.cos(np.radians(theta))


        v_head = np.hypot(vx_head,vy_head) # 2d velocity in x and y plane relative to camera

        ## Align the timings 

        time_video = Video['timestamp [ns]'].to_numpy()/10**6 # Get the timing from the video file 
        NormTime = time_video[0] # Define a common start point, the initial frame of the eye recording 

        # Update the time variables
        time_eye = time_eye-NormTime 
        time_video = time_video-NormTime
        time_head = time_head-NormTime

        ## Define the intervall of the analysis 

        Start = np.min(time_eye)/1000 # Time of start in [s]
        End =  np.max(time_eye) # Time of end in [s]
        

        ## Save the eye velocity 
        ## Save the eye velocity 
        Data = np.zeros((len(time_eye),2))
        Data[:,0] = time_eye
        Data[:,1] = vel_eye
        df = pd.DataFrame(Data,columns = ['time [ms]', 'velocity [deg/s]'])
        df.to_csv(os.path.join(Datapath,Rec,'EyeVelocity.csv'), index=False)

        # ## Label the blinks and look into what the gaze is doing 
        if AnalyzeBlinks:
            print('... Looking at Blinks ...')
    # Define some interval to not run it on the whole code
            on_eye = 0
            off_eye = np.max(np.where(time_eye < End*1000))

            # Get the indexes for the blinks 
            BlinkVek = np.array(Eye['blink id'].to_numpy())
            NumBlink = np.nanmax(BlinkVek)

            BlinkOn = []
            BlinkOff= []
            BlinkOnIdx= []
            BlinkOffIdx= []

            for aa in np.arange(1,NumBlink+1): # Find Blink onsets and offsets
                Check = np.where(BlinkVek ==aa )
                On = np.min(Check[0])
                Off = np.max(Check[0])
                BlinkOn.append(time_eye[On])
                BlinkOff.append(time_eye[Off])
                BlinkOnIdx.append(On)
                BlinkOffIdx.append(Off)

            BlinkLabels = at.compute_parameter_blinks(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],BlinkOn,BlinkOff,BlinkOnIdx,BlinkOffIdx,Datapath,Rec)

        # ## Movement Labelling
        # 
        # For that we will take the position and velocity of the eye and head, to identify movements 
        # 

        # ### The saccade Labelling algogrithm 

        if AnalyzeSacc:
            print('... Looking at Saccades ...')
            # Define some interval to not run it on the whole code
            on_eye = np.min(np.where(time_eye > Start*1000))
            off_eye = np.max(np.where(time_eye < End*1000))

            # Finding potential intervals 
            [Saccon,Saccoff,SaccOnIdx,SaccOffIdx,thr] = at.SaccadeLabelling(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],time_eye[on_eye:off_eye],globals()['WindowLength'],globals()['Threshold'],globals()['MaxCounter'],globals()['MinFix'],globals()['Lambda'],globals()['SamplingRateEye'])

            # Check wethere these are actually saccades 
            [UpdtSaccon,UpdtSaccoff,UpdtSacconIdx,UpdtSaccoffIdx] = at.CheckSacc(SaccOnIdx,SaccOffIdx,Saccon,Saccoff,vel_eye[on_eye:off_eye],xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],time_eye[on_eye:off_eye],globals()["UpperThreshold"],globals()["MinThreshold"])
            
            [UpdtSaccon,UpdtSaccoff,UpdtSacconIdx,UpdtSaccoffIdx] = at.CheckMisses(UpdtSaccon,UpdtSaccoff,UpdtSacconIdx,UpdtSaccoffIdx,vel_eye[on_eye:off_eye],xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],time_eye[on_eye:off_eye],globals()["MissThreshold"])

            # Collect the saccades and respective parameter and save them to the data folder, last argument gives option to see main sequence figure
            SaccLabels = at.compute_parameter_sacc(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],UpdtSaccon,UpdtSaccoff,UpdtSacconIdx,UpdtSaccoffIdx,Datapath,Rec)

        ##############################################
        ##### The smooth eye movement labelling algorithm 

        if AnalyzeSacc and AnalyzeSmooth:
            print('... Finding Smooth Eye Movement Segments ....')
            # Find smooth movements
            [PursOn,PursOff,PursOnIdx,PursOffIdx]= at.FindSmooth(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],time_eye[on_eye:off_eye],UpdtSacconIdx,UpdtSaccoffIdx,globals()['MinDurationPursuit'],globals()['MinVelocityPursuit'],globals()['MinVelocityPrctile'],globals()['SamplingRateEye'])

            if len(PursOn) > 0: # If you found pursuit elements
                # Check the smooth movements and update them 
                [PursOn,PursOff,PursOnIdx,PursOffIdx]= at.CheckSmooth(PursOn,PursOff,PursOnIdx,PursOffIdx,UpdtSacconIdx,globals()['MinFix'])

                # Do this twice so that you catch some intervalls where there are two things to merch 
                [PursOn,PursOff,PursOnIdx,PursOffIdx]= at.CheckSmooth(PursOn,PursOff,PursOnIdx,PursOffIdx,UpdtSacconIdx,globals()['MinFix'])


        ##############################################
        ### The head movement labeling algorithm 

        if AnalyzeHead:
            print('... Finding Head Movement Segments ....')

            # Define some interval to not run it on the whole code
            on_head = np.min(np.where(time_head > Start*1000))
            off_head = np.max(np.where(time_head < End*1000))
            
            [HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx]= at.FindHead(v_head[on_head:off_head],time_head[on_head:off_head],globals()['WindowLengthHead'],globals()['MinVelocityPrctileHead'],globals()['MinVelocityHead'],globals()['MinDurationHead']) # Label the head movements

            [HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx]= at.CheckHead(HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx,globals()['MinFix']) # Check whethter they are valid 

            [HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx]= at.CheckHead(HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx,globals()['MinFix']) # Do it twice


        ##############################################
        #### Combine head and eye movements to classify VOR movements

        if AnalyzeSmooth:

            print('... Distinguishing Pursuit and VOR ....')

            [VOR,Pursuit] = at.CheckVOR(time_eye[on_eye:off_eye],PursOnIdx,PursOffIdx,vel_x[on_eye:off_eye],vel_y[on_eye:off_eye],time_head,vx_head,vy_head,globals()['SamplingRateEye'],globals()['SamplingRateHead'],globals()['DirectionWindow'],globals()['DistanceWindow'])

            # Divide the smooth labels into pursuit and VOR 
            VOROn =PursOn[VOR]
            VOROff =PursOff[VOR]
            VOROnIdx =PursOnIdx[VOR]
            VOROffIdx =PursOffIdx[VOR]

            PursOn = PursOn[Pursuit]
            PursOff = PursOff[Pursuit]
            PursOnIdx = PursOnIdx[Pursuit]
            PursOffIdx = PursOffIdx[Pursuit]

            

        # #### Save the pursuit, VOR and head movement parameter

        ## Now save the parameter of the detected movements 

        print('... Saving Data ....')
        if AnalyzeSmooth:
            at.compute_parameter_purs(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],PursOn,PursOff,PursOnIdx,PursOffIdx,Datapath,Rec)

            at.compute_parameter_vor(xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],vel_eye[on_eye:off_eye],vx_head,vy_head,VOROn,VOROff,VOROnIdx,VOROffIdx,time_head,time_eye[on_eye:off_eye],Datapath,Rec,globals()['SamplingRateHead'])

        if AnalyzeHead: 
            at.compute_parameter_head(vx_head[on_head:off_head],vy_head[on_head:off_head],HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx,Datapath,Rec,globals()['SamplingRateHead'])

        if AnalyzeSacc and AnalyzeSmooth:
            [FixOn, FixOff] = at.compute_parameter_fix(xpos[on_eye:off_eye],ypos[on_eye:off_eye],xdeg[on_eye:off_eye],ydeg[on_eye:off_eye],time_eye[on_eye:off_eye],PursOnIdx,PursOffIdx,UpdtSacconIdx,UpdtSaccoffIdx,VOROnIdx,VOROffIdx,Blink_Vek[on_eye:off_eye],Datapath,Rec,globals()['MinFix'])

        ## Add the Depth Vector ################################
        Start_time = np.min(time_video)/1000 # Time of start in [s]
        End_time =  np.max(time_video)/1000 # Time of end in [s]
                
        Start = np.min(np.where(time_video > Start_time*1000))
        End = np.max(np.where(time_video< End_time*1000))

        DepthVek = at.DepthLabelling(Start,End,time_video,time_eye,xpos,ypos,Datapath,Rec)

        ## Save the Depth Map
        Data = np.zeros((len(time_video[Start:End])+1,2))
        Data[:,0] = time_video[Start-1:End]
        Data[:,1] = DepthVek.ravel()
        df = pd.DataFrame(Data,columns = ['time [ms]', 'Depth [m]'])
        df.to_csv(os.path.join(Datapath,Rec,'DepthMap.csv'), index=False)