
""" Helper function to keep only non nan elements in a vector """
import numpy as np 

def keep_longest_true(a):
    # Convert to array
    a = np.asarray(a)

    # Attach sentients on either sides w.r.t True
    b = np.r_[False,a,False]

    # Get indices of group shifts
    s = np.flatnonzero(b[:-1]!=b[1:])

    # Get group lengths and hence the max index group
    m = (s[1::2]-s[::2]).argmax()

    # Initialize array and assign only the largest True island as True.
    out = np.zeros_like(a)
    out[s[2*m]:s[2*m+1]] = 1
    return out

