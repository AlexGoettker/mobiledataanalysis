
"""Helper Function to refine saccade that are detected as too long """
import numpy as np 

def RefineSacc(VelVek,timeVek,Saccon,Saccoff):
    

    if VelVek.size > 1:
        PeakVel = int(np.array(np.where(np.max(VelVek) == VelVek)))# Get the position of the peak 

         # Find the moment before and after the peak where the velocity is half of the peak 
        check_before = np.array(np.where(VelVek[0:PeakVel] < np.max(VelVek)*0.5))
        check_after = np.array(np.where(VelVek[PeakVel:-1] < np.max(VelVek)*0.5))

        if np.any(check_before) and np.any(check_after):
            Vel_before = np.max(check_before) 
            Vel_after= PeakVel+np.min(check_after)
    
        # Do a regression to find better onset estimate 
            Vel = VelVek[Vel_before:PeakVel+1]
            time_before = timeVek[Vel_before:PeakVel+1]
            p = np.polyfit(time_before,Vel,1)
            EstimateOnset = -p[1]/p[0]

        # Do a regression to find better offset estimate 
            Vel = VelVek[PeakVel:Vel_after+1]
            time_after = timeVek[PeakVel:Vel_after+1]
            p = np.polyfit(time_after,Vel,1)
            EstimateOffset = -p[1]/p[0]

            if abs(EstimateOnset - Saccon) < 200 and  abs(EstimateOffset - Saccoff) < 200 and EstimateOffset > EstimateOnset:
                Saccon_update = EstimateOnset
                Saccoff_update = EstimateOffset
            else: 
                Saccon_update = Saccon
                Saccoff_update = Saccoff
        else:
            Saccon_update = Saccon
            Saccoff_update = Saccoff 
            EstimateOnset = np.nan
            EstimateOffset = np.nan
    else: 
            Saccon_update = Saccon
            Saccoff_update = Saccoff 
            EstimateOnset = np.nan
            EstimateOffset = np.nan
    return Saccon_update,Saccoff_update, EstimateOnset, EstimateOffset





