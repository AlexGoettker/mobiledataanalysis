""" Main Function for Saccade Labelling """
import numpy as np
import AnalysisTools as at
from itertools import compress 

def SaccadeLabelling(xpos,ypos,vel,time,WindowLength,Threshold,MaxCounter,MinFix,Lambda,SamplingRate):

    ## This function defines saccades as outliers in velocity during a fixation phase
    # Input args: 
        # x-position of gaze
        # y-position of gaze
        # gaze 2D Velocity
        # Timing of gaze samples
    # Output args 
        # Fixation on and offset in time and as index
        # Saccade on and offset in time and as index

      
    WindowSize = int(WindowLength/(1000/SamplingRate)) # Get the actual window size you are looking at 

    # Initialize some variables 
    thr= np.zeros(len(time))
    ninwin = np.zeros(len(time))

    #####  Now start with finding velocity thresholds #######

    # detect slow phases with moving window averaged threshold
    nSamp   = len(time)
    lastwinstart = nSamp-WindowSize
    LoopVek = np.arange(0,lastwinstart+1)
    print('...... Finding Thresholds ....')

    for b in LoopVek: # Loop possible 

        vel_temp = vel[b:b+WindowSize] # Get the vector in the current window

        thr2 = at.FindThresholds(vel_temp,Threshold,MaxCounter,Lambda) # Find the thresholds

        thr[b:b+WindowSize] =  thr[b:b+WindowSize]+thr2 # add the computed threshold to the window
        ninwin[b:b+WindowSize] =  ninwin[b:b+WindowSize]+1 # compute how many each time point existed in the window

    thr = thr/ninwin # Get the mean threshold across all instances

    #####  Use the thresholds to find saccades and fixations #######
    
    idx = vel < thr # Find the points in time, eye velocity was below the threshold, and use that to detect fixation onsets and offsets
    
    print('...... Finding Fixations ....')
    [Fixon, Fixoff] = at.DetectSwitches(idx,time,MinFix) # This gives you the on and offset of fixations 

    # Now play the same trick as before to define things in between fixations as saccades

    Labels = np.ones(len(time))    #Get a label vector to mar on and offsets of fixations 

    # Get the indexs for fixation on and offset
    FixOnIdx = np.array(list(compress(range(len(np.in1d(time,Fixon) )), np.in1d(time,Fixon))))# Let them start one frame later to get the end of the saccade
    FixOffIdx =np.array(list(compress(range(len(np.in1d(time,Fixoff) )), np.in1d(time,Fixoff)))) # Let them finish one frame later to get the end of the saccade

    for aa,bb in enumerate(FixOnIdx): # Add them to the label vector
        Labels[FixOnIdx[aa]:FixOffIdx[aa]]= 0

    print('...... Identifiying Saccades ....')

    [Saccon, Saccoff] = at.FindSaccs(Labels,time) # now play the same switch trick to get saccade onset and offsets

    SaccOnIdx = np.array(list(compress(range(len(np.in1d(time,Saccon) )), np.in1d(time,Saccon)))) # Get the indexs for saccade on and offset
    SaccOffIdx =np.array(list(compress(range(len(np.in1d(time,Saccoff) )), np.in1d(time,Saccoff))))

    return Saccon,Saccoff,SaccOnIdx,SaccOffIdx,thr