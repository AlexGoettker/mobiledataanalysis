import numpy as np
import os 
from itertools import count
import pandas as pd 


def compute_parameter_sacc (xdeg,ydeg,vel_eye,UpdtSaccon,UpdtSaccoff,UpdtSacconIdx,UpdtSaccoffIdx,Datapath,Rec):
    
    ## This function takes the saccade labels and produces an output file where all relevant parameters are saved 
    # Input args: 
        # x-position
        # y-position 
        # eye velocity 
        # SaccadeOnsets as Idx
        # SaccadeOffsets as Idx
        # SaccadeOnsets as time
        # Saccoff as time 
    
    # Output args 
        # Panda with saccade parameter:
            # On and offset in frames 
            # On and offset in time 
            # Saccade Duration 
            # Saccade Amplitude
            # Saccade PeakVel
            # Saccade Direction

    
    SaccParam = np.zeros((len(UpdtSaccon),8)) # Initialize the saving array

    for counter, on, off in zip(count(), UpdtSacconIdx, UpdtSaccoffIdx): # Loop through all of the saccades 
        if off > on:
            SaccParam[counter,0] = on # Onset in frames
            SaccParam[counter,1] = off # Offset in frames
            SaccParam[counter,2] = UpdtSaccon[counter] # Onset in time
            SaccParam[counter,3] = UpdtSaccoff[counter] # Offset in time
            SaccParam[counter,4] = UpdtSaccoff[counter]-UpdtSaccon[counter] # Saccade Duration [ms]
            SaccParam[counter,5] = np.sqrt((xdeg[off]-xdeg[on])**2+(ydeg[off]-ydeg[on])**2)  # Saccade Amplitude [deg]
            SaccParam[counter,6] = np.max(vel_eye[on:off])  # Saccade Amplitude [deg]
            SaccParam[counter,7] = np.rad2deg(np.arctan2(ydeg[off]-ydeg[on], xdeg[off]-xdeg[on])) # Saccade Direction [°]


    df = pd.DataFrame(SaccParam,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','Saccade Duration [ms]','Saccade Amplitude [deg]','Saccade PeakVel [deg/s]','Saccade Direction [°]'])
    df.head(n =5)
    df.to_csv(os.path.join(Datapath,Rec,'SaccParam.csv'), index=False)
        
    return df
  