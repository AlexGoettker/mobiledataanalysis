""" Main Function to fine tune the saccade selection 
--> Initial script only finds outliers, now check whether these are actually saccades """

import numpy as np 
import AnalysisTools as at
from itertools import count
from itertools import compress

def CheckSacc(SaccOnIdx,SaccOffIdx,Saccon,Saccoff,vel_eye,xdeg,ydeg,time_eye,UpperThreshold,MinThreshold):

    ## This function checks whether initially identified saccades fullfil some basic criteria
    # Input args: 
        # SaccadeOnsets as Idx
        # SaccadeOffsets as Idx
        # SaccadeOnsets as time
        # Saccoff as time 
        # eye velocity 
        # x-position
        # y-position 
    
    # Output args 
        # Updated Saccades on and offset in time and as index
   
    # Initialize empty arrays and variables 
    ValidSacc = []#
    PeakVel = []
    Amplitude = []
    Duration = []
    SaccAddOn = []
    SaccAddOff = []

    for counter, on, off in zip(count(), SaccOnIdx, SaccOffIdx): # Loop through all of the saccades 
        # Check for saccades who include a lot of NaNs
        if on == off: # Check whether there is a saccade that has the same on and offset
            ValidSacc.append(False)
            PeakVel.append(np.nan) # Peak Vel
            Duration.append(np.nan) # Duration
            Amplitude.append(np.nan) # Compute the amplitude
            continue
        
        if  np.isnan(np.max(vel_eye[on:off])):
            NanSacc = 1
            if on - 5 >  0 and off +5 < len(vel_eye):
                Temp_Vek = np.arange(on-5,off+5)
                idx = at.keep_longest_true(~np.isnan(vel_eye[on-5:off+5]))
                Temp_Vek = Temp_Vek[idx] # Find the longest consecutive streak of valid samples 
            else: 
                Temp_Vek = np.arange(on,off)
                idx = at.keep_longest_true(~np.isnan(vel_eye[on:off]))
                Temp_Vek = Temp_Vek[idx] # Find the longest consecutive streak of valid samples 
            # Update the on and offsets 
            on_new= np.min(Temp_Vek)
            off_new = np.max(Temp_Vek)
            Saccon[counter] = time_eye[on_new]
            Saccoff[counter] = time_eye[off_new]
            SaccOnIdx[counter] = on_new
            SaccOffIdx[counter] = off_new
            
            if off_new-on_new > 5:
            # Get Saccade Parameter
                PeakVel.append(np.max(vel_eye[on_new:off_new])) # Peak Vel
                Duration.append(Saccoff[counter]- Saccon[counter]) # Duration
                Amplitude.append(np.sqrt((xdeg[off_new]-xdeg[on_new])**2+(ydeg[off_new]-ydeg[on_new])**2)) # Compute the amplitude
                Vel_before_rat = vel_eye[on_new]/PeakVel[counter] # Check ratio: velocity before sacc to peak, special case, since you have NaNs before you cant average
                Vel_after_rat = vel_eye[off_new]/PeakVel[counter]# Check ratio: velocity after sacc to peak
                
            else: 
                PeakVel.append(np.nan) # Peak Vel
                Duration.append(np.nan) # Duration
                Amplitude.append(np.nan) # Compute the amplitude
                Vel_before_rat = np.nan # Check ratio: velocity before sacc to peak, special case, since you have NaNs before you cant average
                Vel_after_rat = np.nan# Check ratio: velocity after sacc to peak      
                
        else:
            NanSacc = 0
            # Get Saccade Parameter
            PeakVel.append(np.max(vel_eye[on:off])) # Peak Vel
            Duration.append(Saccoff[counter]- Saccon[counter]) # Duration
            Amplitude.append(np.sqrt((xdeg[off]-xdeg[on])**2+(ydeg[off]-ydeg[on])**2)) # Compute the amplitude

            if on-5 > 0 and off + 5 < len(vel_eye): # Check for potential saccades directly at the beginning or end 
                Vel_before_rat = np.median(vel_eye[on-5:on])/PeakVel[counter] # Check ratio: velocity before sacc to peak
                Vel_after_rat = np.median(vel_eye[off:off+5])/PeakVel[counter]# Check ratio: velocity after sacc to peak
            else: 
                Vel_before_rat = vel_eye[on]/PeakVel[counter] # Check ratio: velocity before sacc to peak
                Vel_after_rat = vel_eye[off]/PeakVel[counter]# Check ratio: velocity after sacc to peak
                
        # if counter > 30 and counter < 40: 
        #     print(counter, on,off,time_eye[on], Duration[counter],PeakVel[counter],Amplitude[counter],Vel_before_rat,Vel_after_rat)
        
###################################################################################################################################
        ## This now checks for your criteria
        if PeakVel[counter] >  MinThreshold and PeakVel[counter] <  UpperThreshold and Vel_before_rat < 0.5 and Vel_after_rat < 0.5 and Amplitude[counter]>0.1: # check whether the saccades fulfill the min requirement
            
            ######################### If it seems a standard saccade ###############################################
            if Duration[counter] > 10 and Duration[counter] <= 100: 
                ValidSacc.append(True) # Just accept it 
        
            ######################### Now check some cases where you detected very long saccades ###############################################
            elif Duration[counter] > 100: ## Look at long ones....
                 
                if NanSacc:
                    VelVek = vel_eye[on_new:off_new] # Get the velocity and the time vektor
                    timeVek = time_eye[on_new:off_new]
                else: 
                    if on-5 > 0 and off + 5 < len(time_eye):
                        VelVek = vel_eye[on-5:off+5] # Get the velocity and the time vektor
                        timeVek = time_eye[on-5:off+5]
                    else: 
                        VelVek = vel_eye[on:off] # Get the velocity and the time vektor
                        timeVek = time_eye[on:off]  


                # Estimate whether there is one or two peaks in it
                Check = np.where(np.diff(np.where(VelVek[5:-5] > 100))>1)

                if len(Check[1]==0): # If there is only one peak 

                    [OnEst,OffEst,OnsetEstimate,OffsetEstimate] = at.RefineSacc(VelVek,timeVek,Saccon[counter],Saccoff[counter]) # adjust the saccade labels
                        
                    # The estimate does not necessaryly belong to an actual time sample... correct that so you have all idx as well 
                    if np.isin(OnEst,time_eye): # check that for the onset 
                        Saccon[counter] = OnEst
                    else: 
                        Index = int(np.min(np.array((np.where(np.abs(time_eye-OnEst)==np.min(np.abs(time_eye-OnEst)))))))# Find the actual temporal sample that is the closest
                        Saccon[counter] = time_eye[Index]
                        
                    if np.isin(OffEst,time_eye): # check that for the offset 
                        Saccoff[counter] = OffEst
                    else: 
                        Index = int(np.min(np.array((np.where(np.abs(time_eye-OffEst)==np.min(np.abs(time_eye-OffEst)))))))# Find the actual temporal sample that is the closest
                        Saccoff[counter] = time_eye[Index]

                    # Recompute Parameter
                
                    OnEst = int(np.min(np.array((np.where(np.abs(timeVek-OnEst)==np.min(np.abs(timeVek-OnEst)))))))# Find the actual temporal sample that is the closest
                    OffEst = int(np.min(np.array((np.where(np.abs(timeVek-OffEst)==np.min(np.abs(timeVek-OffEst)))))))# Find the actual temporal sample that is the closest

                    Pv= np.max(VelVek[OnEst:OffEst]) # Peak Vel
                    Dur = Saccoff[counter]- Saccon[counter] # Duration
                    if on-5 > 0 and off + 5 < len(VelVek): # Check for potential saccades directly at the beginning or end 
                        Vel_before_rat = np.median(VelVek[OnEst-5:OnEst])/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = np.median(VelVek[OffEst:OffEst+5])/Pv# Check ratio: velocity after sacc to peak
                    else: 
                        Vel_before_rat = VelVek[OnEst]/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = VelVek[OffEst]/Pv# Check ratio: velocity after sacc to peak 
                    
                    # if counter > 30 and counter < 40: 
                    #     print('Refining...',counter,  Saccon[counter], Saccoff[counter],Dur,Pv,Vel_before_rat,Vel_after_rat)
                                        
                    if Pv>  MinThreshold and Pv <  UpperThreshold and Vel_before_rat < 0.5 and Vel_after_rat < 0.5 and Dur < 250:
                        ValidSacc.append(True)
                    else: 
                        ValidSacc.append(False)
                    
                else: ## Do it for the ones, where you expect 2 saccades..
                    Orig_On = Saccon[counter]
                    Orig_Off = Saccoff[counter]

                    if NanSacc:
                        Vel_Vek = vel_eye[on_new:off_new] # Get the velocity and the time vektor
                        timeVek = time_eye[on_new:off_new]
                    else: 
                        if on-5 > 0 and off+5 < len(vel_eye): 
                            Vel_Vek = vel_eye[on-5:off+5] # Get the velocity and the time vektor
                            timeVek = time_eye[on-5:off+5]
                        else: 
                            Vel_Vek = vel_eye[on:off] # Get the velocity and the time vektor
                            timeVek = time_eye[on:off]

                    # Do it for the first half 
                    [SaccOn1,SaccOff1,OnsetEstimate,OffsetEstimate] = at.RefineSacc(Vel_Vek[0:int(len(Vel_Vek)/2)],timeVek[0:int(len(Vel_Vek)/2)],Saccon[counter],Saccon[counter]+(Saccoff[counter]-Saccon[counter])/2)

                    if np.isin(SaccOn1,time_eye): # check that for the onset 
                        Saccon[counter] = SaccOn1
                    else: 
                        Index = int(np.array(np.min((np.where(np.abs(time_eye-SaccOn1)==np.min(np.abs(time_eye-SaccOn1)))))))# Find the actual temporal sample that is the closest
                        Saccon[counter] = time_eye[Index]

                    if np.isin(SaccOff1,time_eye): # check that for the onset 
                        Saccoff[counter]  = SaccOff1
                    else: 
                        Index = int(np.array(np.min((np.where(np.abs(time_eye-SaccOff1)==np.min(np.abs(time_eye-SaccOff1)))))))# Find the actual temporal sample that is the closest
                        Saccoff[counter]  = time_eye[Index]
                    
                    # Recompute the parameter
                    SaccOn1 = int(np.array(np.min((np.where(np.abs(timeVek-SaccOn1)==np.min(np.abs(timeVek-SaccOn1)))))))
                    SaccOff1 = int(np.array(np.min((np.where(np.abs(timeVek-SaccOff1)==np.min(np.abs(timeVek-SaccOff1)))))))
                    if SaccOn1 == SaccOff1:
                        SaccOff1 = SaccOff1+1

                    Pv= np.max(Vel_Vek[SaccOn1:SaccOff1]) # Peak Vel
                    Dur = Saccoff[counter]- Saccon[counter] # Duration
                    if on-5 > 0 and off + 5 < len(Vel_Vek): # Check for potential saccades directly at the beginning or end 
                        Vel_before_rat = np.median(Vel_Vek[SaccOn1-5:SaccOn1])/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = np.median(Vel_Vek[SaccOff1:SaccOff1+5])/Pv# Check ratio: velocity after sacc to peak
                    else: 
                        Vel_before_rat = Vel_Vek[SaccOn1]/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = Vel_Vek[SaccOff1]/Pv# Check ratio: velocity after sacc to peak 
                    
                                    
                    if Pv>  MinThreshold and Pv <  UpperThreshold and Vel_before_rat < 0.5 and Vel_after_rat < 0.5 and Dur < 150:
                        ValidSacc.append(True)
                    else: 
                        ValidSacc.append(False)

                    # Do it for the second half                 
                    [SaccOn2,SaccOff2,OnsetEstimate,OffsetEstimate] = at.RefineSacc(Vel_Vek[int(len(Vel_Vek)/2):-1],timeVek[int(len(Vel_Vek)/2):-1],Orig_On+(Orig_Off-Orig_On)/2,Orig_Off)
                                        
                    if np.isin(SaccOn2,time_eye): # check that for the onset 
                        SaccOn2 = SaccOn2
                    else: 
                        Index = int(np.array(np.min((np.where(np.abs(time_eye-SaccOn2)==np.min(np.abs(time_eye-SaccOn2)))))))# Find the actual temporal sample that is the closest
                        SaccOn2 = time_eye[Index]

                    if np.isin(SaccOff2,time_eye): # check that for the onset 
                        SaccOff2 = SaccOff2
                    else: 
                        Index = int(np.array((np.where(np.abs(time_eye-SaccOff2)==np.min(np.abs(time_eye-SaccOff2))))))# Find the actual temporal sample that is the closest
                        SaccOff2 = time_eye[Index]

                    # Recompute the parameter
                    SaccAddOnTime = SaccOn2 # Save the original value
                    SaccAddOffTime = SaccOff2
                    SaccOn2 = int(np.array((np.where(np.abs(timeVek-SaccOn2)==np.min(np.abs(timeVek-SaccOn2)))))) # get the index
                    SaccOff2 = int(np.array((np.where(np.abs(timeVek-SaccOff2)==np.min(np.abs(timeVek-SaccOff2))))))

                    if SaccOn2 == SaccOff2:
                        SaccOff2 = SaccOff2+1
                    Pv= np.max(Vel_Vek[SaccOn2:SaccOff2]) # Peak Vel
                    Dur = Saccoff[counter]- Saccon[counter] # Duration
                    if on-5 > 0 and off + 5 < len(Vel_Vek): # Check for potential saccades directly at the beginning or end 
                        Vel_before_rat = np.median(Vel_Vek[SaccOn2-5:SaccOn2])/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = np.median(Vel_Vek[SaccOff2:SaccOff2+5])/Pv# Check ratio: velocity after sacc to peak
                    else: 
                        Vel_before_rat = Vel_Vek[SaccOn2]/Pv # Check ratio: velocity before sacc to peak
                        Vel_after_rat = Vel_Vek[SaccOff2]/Pv# Check ratio: velocity after sacc to peak 
                    
                
                    if Pv> MinThreshold and Pv <  UpperThreshold and Vel_before_rat < 0.5 and Vel_after_rat < 0.5 and Dur < 150:
                        SaccAddOn.append(SaccAddOnTime)
                        SaccAddOff.append(SaccAddOffTime)  
            else: 
                ValidSacc.append(False)    
           
        else: # If not set the list to nan
            ValidSacc.append(False)

        #print(counter,len(ValidSacc))  
      
    # Now update the saccade lists by the saccades you thought are valid 
    SaccOnUpdate = Saccon[ValidSacc]
    SaccOffUpdate = Saccoff[ValidSacc]

    SaccOnUpdate = np.sort(np.append(SaccOnUpdate,np.array(SaccAddOn)))
    SaccOffUpdate = np.sort(np.append(SaccOffUpdate,np.array(SaccAddOff)))
    UpdtSacconIdx = np.array(list(compress(range(len(np.in1d(time_eye,SaccOnUpdate) )), np.in1d(time_eye,SaccOnUpdate)))) # Get the indexs for saccade on and offset
    UpdtSaccoffIdx = np.array(list(compress(range(len(np.in1d(time_eye,SaccOffUpdate) )), np.in1d(time_eye,SaccOffUpdate)))) # Get the indexs for saccade on and offset


    return SaccOnUpdate,SaccOffUpdate, UpdtSacconIdx, UpdtSaccoffIdx