
def FindThresholds(vel_temp,Threshold,MaxCounter,Lambda): 

    import numpy as np 

    idx = vel_temp<Threshold # Find the indexes of the velocity below threshold, this autmatically exlcudes NaNs

    meanvel = np.mean(vel_temp[idx]) # Compute the mean and std
    stdvel = np.std(vel_temp[idx])

        ## Now try to refine that
    counter = 0 # Initialize a counter variable 
    oldthr  = 0

    while 1: 
        thr2 = meanvel+Lambda*stdvel # Set a running threshold based on mean + Lambda * Std
        qvel = vel_temp < thr2; # Find the idx that are below that    

        if thr2 == oldthr or counter >= MaxCounter:
            break         
        meanvel = np.mean(vel_temp[qvel])
        stdvel = np.std(vel_temp[qvel])
        oldthr = thr2
        counter =+ 1

    thr2 = meanvel+Lambda*stdvel   # determine the final threshold based on data noise during that time 
    return thr2



