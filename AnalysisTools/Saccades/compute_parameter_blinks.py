import numpy as np
import os 
from itertools import count
import pandas as pd 


def compute_parameter_blinks (xdeg,ydeg,vel_eye,Blinkon,Blinkoff,BlinkonIdx,BlinkoffIdx,Datapath,Rec):
    
    ## This function takes the saccade labels and produces an output file where all relevant parameters are saved 
    # Input args: 
        # x-position
        # y-position 
        # eye velocity 
        # BlinkOnsets as Idx
        # BlinkOffsets as Idx
        # BlinkOnsets as time
        # Blinkoff as time 
    
    # Output args 
        # Panda with Blink parameter:
            # On and offset in frames 
            # On and offset in time 
            # Blink Duration 
            # Blink Gaze Shift
            # Blink Gazeshift Direction

    
    BlinkParam = np.zeros((len(Blinkon),7)) # Initialize the saving array

    for counter, on, off in zip(count(), BlinkonIdx, BlinkoffIdx): # Loop through all of the saccades 
        if off > on:
            BlinkParam[counter,0] = on # Onset in frames
            BlinkParam[counter,1] = off # Offset in frames
            BlinkParam[counter,2] = Blinkon[counter] # Onset in time
            BlinkParam[counter,3] = Blinkoff[counter] # Offset in time
            BlinkParam[counter,4] = Blinkoff[counter]-Blinkon[counter] # Saccade Duration [ms]
            BlinkParam[counter,5] = np.sqrt((xdeg[off+1]-xdeg[on-1])**2+(ydeg[off+1]-ydeg[on-1])**2)  # Saccade Amplitude [deg]
            BlinkParam[counter,6] = np.rad2deg(np.arctan2(ydeg[off+1]-ydeg[on-1], xdeg[off+1]-xdeg[on-1])) # Saccade Direction [°]


    df = pd.DataFrame(BlinkParam,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','Blink Duration [ms]','Blink Amplitude [deg]','Blink Direction [°]'])
    df.head(n =5)
    df.to_csv(os.path.join(Datapath,Rec,'BlinkParam.csv'), index=False)
        
    return df
  