""" Main Function to fine tune the saccade selection 
--> Initial script only finds outliers, now check whether these are actually saccades """

import numpy as np 
import AnalysisTools as at
from itertools import count
from itertools import compress

def CheckMisses(SaccOnUpdate,SaccOffUpdate,SaccOnIdx,SaccOffIdx,vel_eye,xdeg,ydeg,time_eye,MissThreshold):

    ## This function checks whether there any residual saccades that are missed
    
    # Input args: 
        # SaccadeOnsets as Idx
        # SaccadeOffsets as Idx
        # eye velocity 
        # x-position
        # y-position 
        # Time
    
    # Output args 
        # Updated Saccades on and offset in time and as index
   
    Labels = np.ones(len(time_eye))    #Get a label vector to mar on and offsets of fixations 
    for idx, on, off in zip(count(), SaccOnIdx, SaccOffIdx):
        Labels[on:off] = 0

    [Fixon, Fixoff] = at.FindSaccs(Labels,time_eye) # now play the same switch trick to find saccade onsets and offsets

    FixOnIdx = np.array(list(compress(range(len(np.in1d(time_eye,Fixon) )), np.in1d(time_eye,Fixon))))# Let them start one frame later to get the end of the saccade
    FixOffIdx =np.array(list(compress(range(len(np.in1d(time_eye,Fixoff) )), np.in1d(time_eye,Fixoff)))) # Let them finish one frame later to get the end of the saccade

    FixOnIdx = FixOnIdx+1 # Add 1 so you get the sample after the saccade offset
    
    
    if FixOnIdx[-1]> FixOffIdx[-1]: # Catch an exception that only the last sample is labelled as fixation--> This will lead to problems later
        FixOnIdx = FixOnIdx[0:-2]
        FixOffIdx = FixOffIdx[0:-2]    


    # Initialize empty arrays and variables       
    SaccOn = []
    SaccOff = []

    for idx, on, off in zip(count(), FixOnIdx, FixOffIdx):

        # Get the onset of the current "fixation" phase
        Start = on
        StartTime = time_eye[on] 
        # Compute the a outlier velocity in this intervall that does not include a saccade (defined as percentile)

        if off-on < 1: 
            continue
        
        vel_time = vel_eye[on:off]
        
        if off < len(vel_eye): # As long as there is the theoretical chance of finding pursuit...

            while 1: # From the start check whether you find enough samples to fullfill your criterion
            
                EndTime = time_eye[Start] # Set the end time to your start time at the beginning
                counter = 1 # Initialize a counter

                if vel_eye[Start] > MissThreshold:# check whether the current velocity is above your thresholds 

                    while 1: # if yes, from the current velocity, search for how long this still is the case ... 
                        # If the velocity is above the threshold, the velocity is not nan and the search is still within the current fixation
                        if (vel_eye[Start+counter] > MissThreshold) and Start+counter < off:
                            counter +=1 # Then add a 1 to the counter

                        else: # if you are outside of this..
                            EndTime =  time_eye[Start+counter] # Update the endtime
                            break
                        
                ## Once you have searches through  
                if StartTime == EndTime: # If the starttime is still the endtime... Then there is no relevant sample
                    Start = Start+counter # Update the start position for the search 
                    if Start < len(time_eye): # If there is still more space to search 
                        StartTime = time_eye[Start] # update the potential start time

                else: # If you did find a relevant sample 
                    PeakVel= np.max(vel_eye[Start:Start+counter]) # Peak Vel
                    Duration=(EndTime- StartTime) # Duration
                    Amplitude= (np.sqrt((xdeg[Start+counter]-xdeg[Start])**2+(ydeg[Start+counter]-ydeg[Start])**2)) # Compute the amplitude
                    Vel_before_rat = np.median(vel_eye[Start-5:Start])/PeakVel # Check ratio: velocity before sacc to peak, special case, since you have NaNs before you cant average
                    Vel_after_rat = np.median(vel_eye[Start+counter:Start+counter+5])/PeakVel# Check ratio: velocity after sacc to peak

                    if Duration > 10 and Duration < 200 and Vel_before_rat < 0.5 and Vel_after_rat < 0.5 and Amplitude>1:  # Check whether the duration is long enough to be counted as pursuit 
                    
                        # Add the saccade by saving the onset and offset 
                        if ~np.isnan(xdeg[Start] and xdeg[Start+counter] ):
                            SaccOn.append(time_eye[Start])
                            SaccOff.append(time_eye[Start+counter])

                        Start = Start+counter+1 # Update the counter to the end of the current saccade epoch
                        if Start < len(time_eye): # If there is still more space to search 
                            StartTime = time_eye[Start] # update the potential start time

                    else: # If saccade doesnt fulfill our criteria
                        Start = Start+counter+1 # Update the start
                        if Start+1 < len(time_eye): # If there is still more space to search 
                            StartTime = time_eye[Start] # update the potential start time

                if Start >= off: # If the start value is at the end of the fixation period.. 
                     break # .. break the search and move on to the next fixation epoch

   
    SaccOnUpdate = np.sort(np.append(SaccOnUpdate,np.array(SaccOn)))
    SaccOffUpdate = np.sort(np.append(SaccOffUpdate,np.array(SaccOff)))
    UpdtSacconIdx = np.array(list(compress(range(len(np.in1d(time_eye,SaccOnUpdate) )), np.in1d(time_eye,SaccOnUpdate)))) # Get the indexs for saccade on and offset
    UpdtSaccoffIdx = np.array(list(compress(range(len(np.in1d(time_eye,SaccOffUpdate) )), np.in1d(time_eye,SaccOffUpdate)))) # Get the indexs for saccade on and offset

    return SaccOnUpdate,SaccOffUpdate, UpdtSacconIdx, UpdtSaccoffIdx