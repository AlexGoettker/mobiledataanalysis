
""" Defintion of helper function """
import numpy as np 

def FindSaccs(idx,time): 
    

    # Now try to find the swicthes 
    data= 0 # Initialize an variable 
    data= np.append(data,idx) # and add zeros to the beginning and end of the data
    data = np.append (data,0)

    # Find the switches by parsing the shifted by one and tacke the difference 
    data11 = data[0:-1]
    data12 = data[1:len(data)]
    numvect	= np.arange(0,len(data11)+1) # This will the vector you can index afterwards

    mdata	= data11 - data12 # Get the difference 

    on= np.where(mdata == -1) # This is the position where there is a onset of a fixation 		
    off= np.where(mdata == 1) # This is the position where there is a offset	

    on2		= numvect[on]        
    off2	= numvect[off]-1

    on = time[on2]                        
    off = time[off2]

    on = np.sort(on)
    off = np.sort(off)
    
    return on,off



