import cv2
import os 
import glob
import numpy as np
from itertools import compress
from itertools import count

def CreateVideoGazeOverlayLabels(Datapath,Resultpath,Rec,Start,End,xpos,ypos,time_eye,time_video,time_head,UpdtSaccon,UpdtSaccoff,PursOn,PursOff,VOROn,VOROff,HeadOn,HeadOff):
  ## This function creates a video of the world camera with gaze overlayed
    # Input args: 
        # Datapath of the folder containing the recordings
        # Resultpath where you want to save the video 
        # Name of the recording
        # Specify in which frame you want to start the recording --> Needs index not time
        # Specify in which frame you want to end the recording --> Needs index not time
        # xpos eye in pixel
        # ypos eye in pixel 
        # time vector the eye samples 
        # time vector of the video frames 
        # Sacc Onsets in Time 
        # Sacc Offsets in Time 
        # Smooth Onsets in Time 
        # Smooth Offsets in Time  
        # VOR Onsets in Time 
        # VOR Offsets in Time  
        # Head Onsets in Time 
        # Head Offsets in Time  
    
    # Output args 
        # None
    # Video file will be saved in the provided folder 
    ##########################################################################

    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    # Prepare the results video
    frame_width = int(cap.get(3)) 
    frame_height = int(cap.get(4)) 
    size = (frame_width, frame_height) # Get the size and match it to input
    framerate = 30 # Set the framerate
    Path = os.path.join(Resultpath,Rec)
   
   # Check for path 
    if os.path.isdir(Path):
        print('Directory exists')
    else: 
        os.mkdir(Path)

    result = cv2.VideoWriter(os.path.join(Resultpath,Rec,'Video_GazeLabels.mp4'),
                         cv2.VideoWriter_fourcc(*'mp4v'), 
                         framerate, size)

    ## Prepare some things for the video 
    # Define the properties of the overlayed circe    
    InnerRadius = 10 # Inner radius of the gaze overlay
    OuterRadius = 30 # Outer radius of the gaze overlay
    ColorGaze = np.array([255,255,0]) # Color of the overlayed element 
    ColorSacc = np.array([255,0,0]) # Color for Saccade bar
    ColorPurs = np.array([0,0,255]) # Color for Smooth bar
    ColorVOR = np.array([255,0,255]) # Color for VOR bar
    ColorHead = np.array([0,255,255]) # Color for Head bar
    ColorBack = np.array([128,128,128]) # Color for bars without movement
    WidthBar = 20 # Width of the color bars

    ################# Create a label vector
    Labels = np.zeros(len(xpos))

    # Get the indexes relative to the total time vector --> Eye Movements
    UpdtSacconIdx = np.array(list(compress(range(len(np.in1d(time_eye,UpdtSaccon) )), np.in1d(time_eye,UpdtSaccon)))) # Get the indexs for saccade on and offset
    UpdtSaccoffIdx = np.array(list(compress(range(len(np.in1d(time_eye,UpdtSaccoff) )), np.in1d(time_eye,UpdtSaccoff)))) # Get the indexs for saccade on and offset
    
    PursOnIdx = np.array(list(compress(range(len(np.in1d(time_eye,PursOn) )), np.in1d(time_eye,PursOn)))) # Get the indexs for saccade on and offset
    PursOffIdx = np.array(list(compress(range(len(np.in1d(time_eye,PursOff) )), np.in1d(time_eye,PursOff)))) # Get the indexs for saccade on and offset

    VOROnIdx = np.array(list(compress(range(len(np.in1d(time_eye,VOROn) )), np.in1d(time_eye,VOROn)))) # Get the indexs for saccade on and offset
    VOROffIdx = np.array(list(compress(range(len(np.in1d(time_eye,VOROff) )), np.in1d(time_eye,VOROff)))) # Get the indexs for saccade on and offset

    for counter, on, off in zip(count(), UpdtSacconIdx, UpdtSaccoffIdx): # Loop through all of the saccades 
        Labels[on:off]=1 
    
    for counter, on, off in zip(count(), PursOnIdx, PursOffIdx): # Loop through all of the saccades 
        Labels[on:off]=2 

    for counter, on, off in zip(count(), VOROnIdx, VOROffIdx): # Loop through all of the saccades 
        Labels[on:off]=3 

    # Get the indexes relative to the total time vector --> Head Movements
    LabelHead = np.zeros(len(time_head))

    HeadOnIdx = np.array(list(compress(range(len(np.in1d(time_head,HeadOn) )), np.in1d(time_head,HeadOn)))) # Get the indexs for saccade on and offset
    HeadOffIdx = np.array(list(compress(range(len(np.in1d(time_head,HeadOff) )), np.in1d(time_head,HeadOff)))) # Get the indexs for saccade on and offset
    
    for counter, on, off in zip(count(), HeadOnIdx, HeadOffIdx): # Loop through all of the saccades 
        LabelHead[on:off]=1
    
    
    counter =0 # Init frame counter
    frame_counter = 0
    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 
    while cap.isOpened():
       
        ret, frame = cap.read()# run through the window...
    
        if counter >= Start and counter <= End: # get only the relevant frames
            img_temp =  np.transpose(frame[:, :, ::-1],(2,1,0)) # Get the image and transform the dimensions so it easier for you to add gaze 

            TimeFrameVideo = [time_video[counter-1]-30, time_video[counter-1]+30] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate
            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window
            idx_head= np.array(np.logical_and(np.greater(time_head,TimeFrameVideo[0]), np.less(time_head,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window
            
            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])

            # Now create a distance map based on gaze 
            X_Vec = np.arange(1,round(img_temp.shape[1]+1))-X
            Y_Vec = np.arange(1,round(img_temp.shape[2]+1))-Y
            x,y = np.meshgrid(Y_Vec,X_Vec)
            Dist = np.sqrt(x**2+y**2)
                       
            # Find which pixels in the image need to be changed
            circ = np.array(np.where((Dist > InnerRadius) & (Dist < OuterRadius)))
            test = img_temp[:,circ[0],circ[1]]
            img_temp[:,circ[0],circ[1]] =np.tile(ColorGaze,(len(circ[0]),1)).T

            ## Add some bars on the top and bottom for movement labels 
            if np.any(Labels[idx]==1): # If you have a saccade
                Idx = np.arange(0,WidthBar)
                img_temp[:,:,Idx] = np.tile(ColorSacc,(len(Idx),np.size(img_temp,1),1)).T
            else: # if not
                Idx = np.arange(0,WidthBar)
                img_temp[:,:,Idx] = np.tile(ColorBack,(len(Idx),np.size(img_temp,1),1)).T

            if np.any(Labels[idx]==2): # if you have a pursuit movement
                Idx = np.arange(np.size(img_temp,1)-WidthBar,np.size(img_temp,1))
                img_temp[:,Idx,:] = np.tile(ColorPurs,(np.size(img_temp,2),len(Idx),1)).T
            else: 
                Idx = np.arange(np.size(img_temp,1)-WidthBar,np.size(img_temp,1))
                img_temp[:,Idx,:] = np.tile(ColorBack,(np.size(img_temp,2),len(Idx),1)).T

            if np.any(Labels[idx]==3): # if you have a VOR movement
                Idx = np.arange(0,WidthBar)
                img_temp[:,Idx,:] = np.tile(ColorVOR,(np.size(img_temp,2),len(Idx),1)).T
            else: 
                Idx = np.arange(0,WidthBar)
                img_temp[:,Idx,:] = np.tile(ColorBack,(np.size(img_temp,2),len(Idx),1)).T

            if np.any(LabelHead[idx_head]==1): # if you have a head movement              
                Idx = np.arange(np.size(img_temp,2)-WidthBar,np.size(img_temp,2))
                img_temp[:,:,Idx] = np.tile(ColorHead,(len(Idx),np.size(img_temp,1),1)).T
            else:
                Idx = np.arange(np.size(img_temp,2)-WidthBar,np.size(img_temp,2))
                img_temp[:,:,Idx] = np.tile(ColorBack,(len(Idx),np.size(img_temp,1),1)).T

                
            ## Convert the image back to the original format 
            img =  np.transpose(img_temp,(2,1,0))

            # Add the resulting image to the video file 
            result.write(img[:, :, ::-1]) 
            print('You have ', frame_counter+1, 'frames out of', (End-Start)+1)
            frame_counter +=1


        # Show and update the counter
        counter +=1 
        # If you readed the end of the relevant frames you can stop
        if counter > End: 
            break
    
    # Save the videos 
    cap.release()
    result.release()
    cv2.destroyAllWindows()