import cv2
import os 
import numpy as np
import torch 
import glob
from unidepth.models import UniDepthV1
from PIL import Image
import matplotlib.pyplot as plt


def CreateVideoGazeDepth(Datapath,Resultpath,Rec,Start,End,xpos,ypos,time_eye,time_video,NormDistance):
  ## This function creates a video of the world camera with gaze overlayed
    # Input args: 
        # Datapath of the folder containing the recordings
        # Resultpath where you want to save the video 
        # Name of the recording
        # Specify in which frame you want to start the recording --> Needs index not time
        # Specify in which frame you want to end the recording --> Needs index not time
        # xpos eye in pixel
        # ypos eye in pixel 
        # time vector the eye samples 
        # time vector of the video frames 
        # Norm Distance that you want to set to black
    
    # Output args 
        # None
    # Video file will be saved in the provided folder 
    ##########################################################################
    
    # Start the model 
    
    # Get the basic instantiation
    model = UniDepthV1.from_pretrained("lpiccinelli/unidepth-v1-vitl14") # or "lpiccinelli/unidepth-v1-cnvnxtl" for the ConvNext backbone

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)


    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    # Prepare the results video
    frame_width = int(cap.get(3)) 
    frame_height = int(cap.get(4)) 
    size = (frame_width, frame_height) # Get the size and match it to input
    framerate = 30 # Set the framerate

     # Check for path 
    Path = os.path.join(Resultpath,Rec)
    if os.path.isdir(Path):
        print('Directory exists')
    else: 
        os.mkdir(Path)
        
    result = cv2.VideoWriter(os.path.join(Resultpath,Rec,'Depth_Gaze.mp4'),
                         cv2.VideoWriter_fourcc(*'mp4v'), 
                         framerate, size)

    ## Prepare some things for the video 
    # Define the properties of the overlayed circe    
    InnerRadius = 10 # Inner radius of the gaze overlay
    OuterRadius = 30 # Outer radius of the gaze overlay
    counter =0 # Init frame counter
    frame_counter = 0

    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 
    while cap.isOpened():
        ret, frame = cap.read()# run through the window...
    
        if counter >= Start and counter <= End: # get only the relevant frames

            # Turn the image into a depthmap 
            img = frame[:, :, ::-1]
            #fig, (ax1, ax2) = plt.subplots(1,2)
            #ax1.imshow(img)

            rgb = torch.from_numpy(img.copy()).permute(2, 0, 1) # C, H, W
            predictions = model.infer(rgb)
            depth = predictions["depth"] # Metric Depth Estimation
            depthmap = torch.Tensor.cpu(depth).numpy() # get the depthmap 
            depthmap = depthmap[0,0,:,:] 

            TimeFrameVideo = [time_video[counter-1]-15, time_video[counter-1]+15] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate

            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window

            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])

            # Now create a distance map based on gaze 
            # Add the gaze position 
            X_Vec = np.arange(1,round(depthmap.shape[1]+1))-X
            Y_Vec = np.arange(1,round(depthmap.shape[0]+1))-Y
            x,y = np.meshgrid(X_Vec,Y_Vec)
            Dist = np.sqrt(x**2+y**2)

            # Find which pixels in the image need to be changed
            circ = np.array(np.where((Dist > InnerRadius) & (Dist < OuterRadius)))
            test = depthmap[circ[0],circ[1]]
            depthmap[circ[0],circ[1]] =0 
            depthmap = (depthmap/NormDistance)*255
            depthmap[depthmap > 255] = 255

            #ax2.imshow(depthmap)

            ## Convert the image back to the original format 
            img = depthmap
            img_write = np.zeros((depthmap.shape[0],depthmap.shape[1],3))
            img_write[:,:,0] = img 
            img_write[:,:,1] = img 
            img_write[:,:,2] = img 

            # Add the resulting image to the video file 
            result.write(np.uint8(img_write))
            print('You have ', frame_counter+1, 'frames out of', (End-Start)+1)
            frame_counter+=1


        # Show and update the counter
        counter +=1

        # If you readed the end of the relevant frames you can stop
        if counter > End:
            break

    # Save the videos 
    cap.release()
    result.release()
    cv2.destroyAllWindows()