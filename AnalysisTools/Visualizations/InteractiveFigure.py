
"""Interactive figure to visualize labelling"""

import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.patches as pltrec
from matplotlib.widgets import Slider, Button, CheckButtons
import ipympl 

def CreateInteractiveFigure(time_eye,xdeg,ydeg,vel_eye,time_head,vx_head,vy_head,vz_head,UpdtSaccon,UpdtSaccoff,PursOn,PursOff,HeadOnset,HeadOffset,VOROn,VOROff,FixOn,FixOff):

    fig, (ax1,ax2,ax3) = plt.subplots(3,figsize=(10,5)) # Create a figure
    fig.subplots_adjust(left=0.1, bottom=0.25)

    # For the first panel 
    ax1.plot(time_eye/1000,xdeg,'-',linewidth=2,color = [0, 0, 1],label = 'X-Position')
    ax1.plot(time_eye/1000,ydeg,'-',linewidth=2,color = [0, 1, 1],label = 'Y-Position')
    ax1.set_ylabel('Position [deg]')
    ax1.legend
    ax1.set_xlim([np.min(time_eye)/1000,np.min(time_eye)/1000+2])
    ax1.set_ylim([-40,30])

    # For the second panel 
    ax2.plot(time_eye/1000,vel_eye,color = [0.2, 0.2, 0.2])
    ax2.set_xlabel('Time [s]')
    ax2.set_ylabel('Velocity [deg/s]')
    ax2.set_xlim([np.min(time_eye)/1000,np.min(time_eye)/1000+2])
    ax2.set_ylim([0,300])

    # For the third panel 
    ax3.plot(time_head/1000,vx_head,color = [0, 0, 1],label = 'X-Velocity')
    ax3.plot(time_head/1000,vy_head,color = [0, 1, 1],label = 'Y-Velocity')
    ax3.plot(time_head/1000,vz_head,color = [1, 0, 1],label = 'Z-Velocity')
    ax3.set_xlabel('Time [s]')
    ax3.set_ylabel('Head Velocity [deg/s]')
    ax3.set_xlim([np.min(time_eye)/1000,np.min(time_eye)/1000+2])
    ax3.set_ylim([-100,100])


    # Add the slider controlling the time 
    axfreq = fig.add_axes([0.1, 0.1, 0.65, 0.03])
    time_slider = Slider(
         ax=axfreq,
         label='Time',
         valmin=np.min(time_eye)/1000+1,
         valmax=np.max(time_eye)/1000-1,
         valinit=1,
    )

    def update(val): # Update the figures 
        ax1.set_xlim([time_slider.val-1,time_slider.val+1])
        ax2.set_xlim([time_slider.val-1,time_slider.val+1])
        ax3.set_xlim([time_slider.val-1,time_slider.val+1])
        Checks = check.get_status()
           
        fig.canvas.draw_idle()

    # Add a button for saccades 
    rax = fig.add_axes([0.83, 0.05, 0.16, 0.1])
    check = CheckButtons(
        ax= rax, labels = ('Saccade','Sacc+Smooth','Complete Labels')   
    )

    def UpdateFigure(label):
        Checks = check.get_status()
        if Checks[0]: # If you want to see the saccade
            for aa,bb in enumerate(UpdtSaccon): # add the labelled saccades
                    Left = UpdtSaccon[aa]/1000
                    # On the position trace
                    rect= pltrec.Rectangle((Left,-40), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,70, color = 'red',alpha = 0.5)
                    ax1.add_patch(rect)
                    # On the Velocity trace
                    rect= pltrec.Rectangle((Left,0), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,300, color = 'red',alpha = 0.5)
                    ax2.add_patch(rect)
               
                
        if Checks[1]: # If you want to see the saccade
            # Add the saccade boxes
            for aa,bb in enumerate(UpdtSaccon): # add the labelled saccades
                    Left = UpdtSaccon[aa]/1000
                    # On the position trace
                    rect= pltrec.Rectangle((Left,-40), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,70, color = 'red',alpha = 0.5)
                    ax1.add_patch(rect)
                    # On the Velocity trace
                    rect= pltrec.Rectangle((Left,0), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,300, color = 'red',alpha = 0.5)
                    ax2.add_patch(rect)
            
            for aa,bb in enumerate(PursOn): # add the labelled saccades
                Left = PursOn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((PursOff[aa]-PursOn[aa]))/1000,70, color = 'blue',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((PursOff[aa]-PursOn[aa]))/1000,300, color = 'blue',alpha = 0.3)
                ax2.add_patch(rect)

            for aa,bb in enumerate(VOROn): # add the labelled saccades
                Left = VOROn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((VOROff[aa]-VOROn[aa]))/1000,70, color = 'blue',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((VOROff[aa]-VOROn[aa]))/1000,300, color = 'blue',alpha = 0.3)
                ax2.add_patch(rect)
                

        if Checks[2]: # If you want to see the saccade
            # Add the saccade boxes
            for aa,bb in enumerate(HeadOnset): # add the labelled saccades
                Left = HeadOnset[aa]/1000
              
                rect= pltrec.Rectangle((Left,0), ((HeadOffset[aa]-HeadOnset[aa]))/1000,100, color = 'yellow',alpha = 0.3)
                ax3.add_patch(rect)

            for aa,bb in enumerate(UpdtSaccon): # add the labelled saccades
                    Left = UpdtSaccon[aa]/1000
                    # On the position trace
                    rect= pltrec.Rectangle((Left,-40), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,70, color = 'red',alpha = 0.5)
                    ax1.add_patch(rect)
                    # On the Velocity trace
                    rect= pltrec.Rectangle((Left,0), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,300, color = 'red',alpha = 0.5)
                    ax2.add_patch(rect)
            
            for aa,bb in enumerate(PursOn): # add the labelled saccades
                Left = PursOn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((PursOff[aa]-PursOn[aa]))/1000,70, color = 'blue',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((PursOff[aa]-PursOn[aa]))/1000,300, color = 'blue',alpha = 0.3)
                ax2.add_patch(rect)

            for aa,bb in enumerate(VOROn): # add the labelled saccades
                Left = VOROn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((VOROff[aa]-VOROn[aa]))/1000,70, color = 'green',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((VOROff[aa]-VOROn[aa]))/1000,300, color = 'green',alpha = 0.3)
                ax2.add_patch(rect)

            for aa,bb in enumerate(FixOn): # add the labelled saccades
                Left = FixOn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((FixOff[aa]-FixOn[aa]))/1000,70, color = 'gray',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((FixOff[aa]-FixOn[aa]))/1000,300, color = 'gray',alpha = 0.3)
                ax2.add_patch(rect)
                
        
        if Checks[0] ==0 and Checks[1] ==0 and Checks[2] ==0: ## if you dont want to see them, just set them to white
            [p.remove() for p in reversed(ax1.patches)]
            [p.remove() for p in reversed(ax2.patches)]
            [p.remove() for p in reversed(ax3.patches)]

       
    time_slider.on_changed(update)
    check.on_clicked(UpdateFigure)
