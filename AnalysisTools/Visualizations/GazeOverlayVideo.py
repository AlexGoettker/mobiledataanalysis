import cv2
import os 
import numpy as np
import glob

def CreateVideoGazeOverlay(Datapath,Resultpath,Rec,Start,End,xpos,ypos,time_eye,time_video):
  ## This function creates a video of the world camera with gaze overlayed
    # Input args: 
        # Datapath of the folder containing the recordings
        # Resultpath where you want to save the video 
        # Name of the recording
        # Specify in which frame you want to start the recording --> Needs index not time
        # Specify in which frame you want to end the recording --> Needs index not time
        # xpos eye in pixel
        # ypos eye in pixel 
        # time vector the eye samples 
        # time vector of the video frames 
    
    # Output args 
        # None
    # Video file will be saved in the provided folder 
    ##########################################################################

    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    # Prepare the results video
    frame_width = int(cap.get(3)) 
    frame_height = int(cap.get(4)) 
    size = (frame_width, frame_height) # Get the size and match it to input
    framerate = 30 # Set the framerate

    # Check for path 
    Path = os.path.join(Resultpath,Rec)
    if os.path.isdir(Path):
        print('Directory exists')
    else: 
        os.mkdir(Path)


    result = cv2.VideoWriter(os.path.join(Resultpath,Rec,'Video_Gaze.mp4'),
                         cv2.VideoWriter_fourcc(*'mp4v'), 
                         framerate, size)

    ## Prepare some things for the video 
    # Define the properties of the overlayed circe    
    InnerRadius = 10 # Inner radius of the gaze overlay
    OuterRadius = 30 # Outer radius of the gaze overlay
    ColorGaze = np.array([255,255,0]) # Color of the overlayed element 
    counter =0 # Init frame counter
    frame_counter = 0

    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 
    while cap.isOpened():
        ret, frame = cap.read()# run through the window...
       
        if counter >= Start and counter <= End: # get only the relevant frames
            img_temp =  np.transpose(frame[:, :, ::-1],(2,1,0)) # Get the image and transform the dimensions so it easier for you to add gaze 

            TimeFrameVideo = [time_video[counter-1]-15, time_video[counter-1]+15] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate

            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window

            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])

            # Now create a distance map based on gaze 
            X_Vec = np.arange(1,round(img_temp.shape[1]+1))-X
            Y_Vec = np.arange(1,round(img_temp.shape[2]+1))-Y
            x,y = np.meshgrid(Y_Vec,X_Vec)
            Dist = np.sqrt(x**2+y**2)
                       
            # Find which pixels in the image need to be changed
            circ = np.array(np.where((Dist > InnerRadius) & (Dist < OuterRadius)))
            test = img_temp[:,circ[0],circ[1]]
            img_temp[:,circ[0],circ[1]] =np.tile(ColorGaze,(len(circ[0]),1)).T

            ## Convert the image back to the original format 
            img = np.transpose(img_temp,(2,1,0))

            # Add the resulting image to the video file 
            result.write(img[:, :, ::-1]) 
            print('You have ', frame_counter+1, 'frames out of', (End-Start)+1)
            frame_counter+=1


        # Show and update the counter
        counter +=1

        # If you readed the end of the relevant frames you can stop
        if counter > End:
            break

    # Save the videos 
    cap.release()
    result.release()
    cv2.destroyAllWindows()