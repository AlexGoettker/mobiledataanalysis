
import matplotlib.animation as animation
import matplotlib.pyplot as plt

def VisualizeWorldVector(headings_in_world,cart_gazes_in_world,frame):

    ##Function to visualie the head and gaze vector in sperhical world coordinates
    # Input args: 
        # headings in world coordinates 
        # gaze in world coordinates
        # frame you want to analyze

    fig, axs = plt.subplots(1, 2, figsize=(10.5, 5))


    # Overhead visualization
    axs[0].axis("square")
    axs[0].set_aspect("equal", adjustable="box")
    axs[0].set_xlim(-1.29, 1.29)
    axs[0].set_ylim(-1.29, 1.29)

    axs[0].axis("off")

    circle = plt.Circle((0, 0), 1.0, color="black", fill=False)
    axs[0].add_patch(circle)

    axs[0].text(0, 1.11, "N", ha="center", va="bottom", fontsize="xx-large")
    axs[0].text(0, -1.11, "S", ha="center", va="top", fontsize="xx-large")
    axs[0].text(1.11, 0, "E", ha="left", va="center", fontsize="xx-large")
    axs[0].text(-1.11, 0, "W", ha="right", va="center", fontsize="xx-large")

    axs[0].plot([0, 0], [1.0, 1.08], color="black")
    axs[0].plot([0, 0], [-1.0, -1.08], color="black")
    axs[0].plot([1.0, 1.08], [0, 0], color="black")
    axs[0].plot([-1.0, -1.08], [0, 0], color="black")

    heading_quiver_overhead = axs[0].quiver(
        0,
        0,
        headings_in_world[frame, 0],
        headings_in_world[frame, 1],
        color="b",
        label="IMU Heading",
        scale=1,
        scale_units="xy",
        angles="xy",
        width=0.01,
    )

    gaze_quiver_overhead = axs[0].quiver(
        0,
        0,
        cart_gazes_in_world[frame, 0],
        cart_gazes_in_world[frame, 1],
        color="r",
        label="Gaze vector",
        scale=1,
        scale_units="xy",
        angles="xy",
        width=0.01,
    )

    axs[0].legend(loc="lower right", prop={"size": 11})

    axs[0].set_title("Heading and Gaze in World - Overhead", fontsize="xx-large")


    # Side profile visualization

    axs[1].axis("square")
    axs[1].set_aspect("equal", adjustable="box")
    axs[1].set_xlim(-1.29, 1.29)
    axs[1].set_ylim(-1.29, 1.29)

    axs[1].axis("off")

    circle = plt.Circle((0, 0), 1.0, color="black", fill=False)
    axs[1].add_patch(circle)

    axs[1].plot([0, 0], [1.0, 1.08], color="black")
    axs[1].plot([0, 0], [-1.0, -1.08], color="black")

    axs[1].text(0, 1.11, "Sky", ha="center", va="bottom", fontsize="xx-large")
    axs[1].text(0, -1.11, "Earth", ha="center", va="top", fontsize="xx-large")

    axs[1].hlines(0, -1.0, 1.0, color="black", linestyle="--")

    heading_quiver_sideprofile = axs[1].quiver(
        0,
        0,
        headings_in_world[frame, 1],
        headings_in_world[frame, 2],
        color="b",
        scale=1,
        scale_units="xy",
        angles="xy",
        width=0.01,
    )

    gaze_quiver_sideprofile = axs[1].quiver(
        0,
        0,
        cart_gazes_in_world[frame, 1],
        cart_gazes_in_world[frame, 2],
        color="r",
        scale=1,
        scale_units="xy",
        angles="xy",
        width=0.01,
    )

    axs[1].set_title("Heading and Gaze in World - Side-profile", fontsize="xx-large")


    fig.tight_layout()

