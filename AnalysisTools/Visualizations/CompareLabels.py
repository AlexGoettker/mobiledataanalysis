
"""Interactive figure to visualize labelling"""

import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.patches as pltrec
from matplotlib.widgets import Slider, Button, CheckButtons
import ipympl 

def CompareLabels(time_eye,xdeg,ydeg,vel_eye,UpdtSaccon,UpdtSaccoff,PursOn,PursOff):

    fig, (ax1,ax2) = plt.subplots(2,figsize=(10,5)) # Create a figure
    fig.subplots_adjust(left=0.1, bottom=0.25)

    # For the first panel 
    ax1.plot(time_eye/1000,xdeg,'-',linewidth=2,color = [0, 0, 1],label = 'X-Position')
    ax1.plot(time_eye/1000,ydeg,'-',linewidth=2,color = [0, 1, 1],label = 'Y-Position')
    ax1.set_ylabel('Position [deg]')
    ax1.legend
    ax1.set_xlim([np.min(time_eye)/1000,np.min(time_eye)/1000+2])
    ax1.set_ylim([-40,30])

    # For the second panel 
    ax2.plot(time_eye/1000,vel_eye,color = [0.2, 0.2, 0.2])
    ax2.set_xlabel('Time [s]')
    ax2.set_ylabel('Velocity [deg/s]')
    ax2.set_xlim([np.min(time_eye)/1000,np.min(time_eye)/1000+2])
    ax2.set_ylim([0,300])
    

    # Add the slider controlling the time 
    axfreq = fig.add_axes([0.1, 0.1, 0.65, 0.03])
    time_slider = Slider(
         ax=axfreq,
         label='Time',
         valmin=np.min(time_eye)/1000+1,
         valmax=np.max(time_eye)/1000-1,
         valinit=1,
    )

    def update(val): # Update the figures 
        ax1.set_xlim([time_slider.val-1,time_slider.val+1])
        ax2.set_xlim([time_slider.val-1,time_slider.val+1])
        Checks = check.get_status()
           
        fig.canvas.draw_idle()

    # Add a button for saccades 
    rax = fig.add_axes([0.83, 0.05, 0.16, 0.1])
    check = CheckButtons(
        ax= rax, labels = ('Own','Pupil Labs','Remove Labels')   
    )

    def UpdateFigure(label):
        Checks = check.get_status()
        if Checks[0]: # If you want to see the saccade
            for aa,bb in enumerate(UpdtSaccon): # add the labelled saccades
                    Left = UpdtSaccon[aa]/1000
                    # On the position trace
                    rect= pltrec.Rectangle((Left,-40), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,70, color = 'red',alpha = 0.5)
                    ax1.add_patch(rect)
                    # On the Velocity trace
                    rect= pltrec.Rectangle((Left,0), ((UpdtSaccoff[aa]-UpdtSaccon[aa]))/1000,300, color = 'red',alpha = 0.5)
                    ax2.add_patch(rect)
               
                
        if Checks[1]: # If you want to see the saccade
            # Add the saccade boxes           
            
            for aa,bb in enumerate(PursOn): # add the labelled saccades
                Left = PursOn[aa]/1000
              
                rect= pltrec.Rectangle((Left,-40), ((PursOff[aa]-PursOn[aa]))/1000,70, color = 'blue',alpha = 0.3)
                ax1.add_patch(rect)

                rect= pltrec.Rectangle((Left,0), ((PursOff[aa]-PursOn[aa]))/1000,300, color = 'blue',alpha = 0.3)
                ax2.add_patch(rect)  
              

                       
        
        if Checks[2] ==1: ## if you dont want to see them, just set them to white
            [p.remove() for p in reversed(ax1.patches)]
            [p.remove() for p in reversed(ax2.patches)]

       
    time_slider.on_changed(update)
    check.on_clicked(UpdateFigure)
