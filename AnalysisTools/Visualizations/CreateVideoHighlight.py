import cv2 
import numpy as np
import os 
import glob
import matplotlib.pyplot as plt

import torch
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger() # Run the setup logger 
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog


def CreateVideoHighlightObject(Datapath,Resultpath,Rec,Start,End,xpos,ypos,time_eye,time_video,SystemName,LabelList,Text):
  ## This function creates a video of the world camera with gaze overlayed & and the fixated Object is highlighted 
    # Input args: 
        # Datapath of the folder containing the recordings
        # Resultpath where you want to save the video 
        # Name of the recording
        # Specify in which frame you want to start the recording --> Needs index not time
        # Specify in which frame you want to end the recording --> Needs index not time
        # xpos eye in pixel
        # ypos eye in pixel 
        # time vector the eye samples 
        # time vector of the video frames 
        # System Name for using different versions of Detectron 
        # LabelList which contains a lookup table
        # Text flag, if you want a text label on the video 
    
    # Output args 
        # None
    # Video file will be saved in the provided folder 
    ##########################################################################

    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    # Prepare the results video
    frame_width = int(cap.get(3)) 
    frame_height = int(cap.get(4)) 
    size = (frame_width, frame_height) # Get the size and match it to input
    framerate = 30 # Set the framerate

     # Check for path 
    Path = os.path.join(Resultpath,Rec)
    if os.path.isdir(Path):
        print('Directory exists')
    else: 
        os.mkdir(Path)

    result = cv2.VideoWriter(os.path.join(Resultpath,Rec,'Video_Highlight.mp4'),
                         cv2.VideoWriter_fourcc(*'mp4v'), 
                         framerate, size)

    ## Prepare some things for the video 
    # Define the properties of the overlayed circe    
    InnerRadius = 10 # Inner radius of the gaze overlay
    OuterRadius = 30 # Outer radius of the gaze overlay
    ColorGaze = np.array([255,0,0]) # Color of the overlayed element 
    ColorObject = np.array([255,255,0]) # Color of the overlayed element 
    OpacityObject = 0.3
    counter =0 # Init frame counter
    frame_counter = 0 
    
    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 
    while cap.isOpened():
        ret, frame = cap.read()# run through the window...
    
        if counter >= Start and counter <= End: # get only the relevant frames

            frame = frame[:, :, ::-1]# get them into rgb
            ################### Gaze ###################################
            img_temp =  np.transpose(frame,(2,1,0)) # Get the image and transform the dimensions so it easier for you to add gaze 

            TimeFrameVideo = [time_video[counter-1]-15, time_video[counter-1]+15] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate

            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window

            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])

            # Now create a distance map based on gaze 
            X_Vec = np.arange(1,round(img_temp.shape[1]+1))-X
            Y_Vec = np.arange(1,round(img_temp.shape[2]+1))-Y
            x,y = np.meshgrid(Y_Vec,X_Vec)
            Dist = np.sqrt(x**2+y**2)
                       
            # Find which pixels in the image need to be changed
            circ = np.array(np.where((Dist > InnerRadius) & (Dist < OuterRadius)))


            ################# Image Processing ############################

            plt.imshow(frame) # Show the image 

            # Inference with a panoptic segmentation model
            cfg = get_cfg() # Get the image to Detectron

            ############
            if SystemName != 'linux':
                cfg.MODEL.DEVICE = 'cpu' # This is here to make it run on mac, not needed when running it with regular gpu
            ############

            #Load the detault weigths
            cfg.merge_from_file(model_zoo.get_config_file("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")) 
            cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")

            # Predict the labels 
            predictor = DefaultPredictor(cfg)
            panoptic_seg, segments_info = predictor(frame)["panoptic_seg"]

            # Now get at the labels 
            LabelPositions = torch.Tensor.cpu(panoptic_seg).numpy().astype('int') # save the pixel masks

            # Get the object labels 
            Objects = np.arange(0,len(segments_info),1)
            Mat = np.zeros((len(segments_info),4))

            for obj in Objects: 
                Mat[obj,0] = segments_info[obj].get("id")
                Mat[obj,1] = segments_info[obj].get("score")
                Mat[obj,2] = segments_info[obj].get("category_id")
                Mat[obj,3] = segments_info[obj].get("instance_id")

            
            ## Look at which objects are fixated 
            fixpix = np.array(np.where((Dist.T < 1)))
            FixatedObjects = np.unique(LabelPositions[fixpix[0],fixpix[1]])
            #if len(FixatedObjects) > 1: # For illustration only pick one object 
            #    FixatedObjects = FixatedObjects[1]
            #print(FixatedObjects)

            if len(FixatedObjects) == 1: 

                if np.isnan(Mat[FixatedObjects-1,1]):
                    FixCat = int(Mat[FixatedObjects-1,2]+79) # This is the belonging entry in the category list 
                else:
                    FixCat = int(Mat[FixatedObjects-1,2]) # This is the belonging category in the list 
    
                # Read the information from the Lookupfile 
                Info = LabelList.iloc[FixCat] # This gives you the relevant information from the list 
                Infostr1 = 'Fixated is: '+ Info['name'] 
                Infostr2 = 'Supercategory: ', Info['supercategory']
                
                ################# Megre them together for illustration #########################
                
                objectFixated = np.array(np.where(LabelPositions.T==FixatedObjects))
                img_temp[:,objectFixated[0],objectFixated[1]] = (img_temp[:,objectFixated[0],objectFixated[1]])*(1-OpacityObject)+OpacityObject*(np.tile(ColorObject,(len(objectFixated[0]),1)).T) # Modify the object
                img_temp[:,circ[0],circ[1]] =np.tile(ColorGaze,(len(circ[0]),1)).T # add the gaze

            ## Convert the image back to the original format 
            img =  np.transpose(img_temp,(2,1,0))

            # Add the resulting image to the video file 
            if Text:
                font = cv2.FONT_HERSHEY_SIMPLEX
                img = np.ascontiguousarray(img, dtype=np.uint8)
                cv2.putText(img,Infostr1,(10,100), font, 2,(0,0,0),2,cv2.LINE_AA)

            result.write(img[:, :, ::-1]) 
            print('You have ', frame_counter+1, 'frames out of', (End-Start)+1)
            frame_counter +=1       
            plt.close('All')

        # update the counter
        counter +=1 

        # If you readed the end of the relevant frames you can stop
        if counter > End: 
            break
    
    # Save the videos 
    cap.release()
    result.release()
    cv2.destroyAllWindows()
    plt.close('all')