"""Main Function for pursuit labelling """
import numpy as np 
from itertools import compress
from itertools import count
import AnalysisTools as at

def FindSmooth(xdeg,ydeg,vel_eye,time_eye,SaccOnIdx,SaccOffIdx,MinDuration,MinVelocity,MinPrctile,Samplingrate):
    
    ## This function finds smooth eye movement epochs in the times that are not labelled saccades 
    # Input args: 
        # x-position of gaze
        # y-position of gaze
        # gaze 2D Velocity
        # Timing of gaze samples
        # Saccade Onsets as Index
        # Saccade Offsets as Indec
    
    # Output args 
        # Pursuit on and offset in time 
        # Pursuit on and offset as index 
    
    # Get a vector that finds all timepoints where there is a saccade
    Labels = np.ones(len(time_eye))    #Get a label vector to mar on and offsets of fixations 
    for idx, on, off in zip(count(), SaccOnIdx, SaccOffIdx):
        Labels[on:off] = 0

    [Fixon, Fixoff] = at.FindSaccs(Labels,time_eye) # now play the same switch trick to find saccade onsets and offsets

    FixOnIdx = np.array(list(compress(range(len(np.in1d(time_eye,Fixon) )), np.in1d(time_eye,Fixon))))# Let them start one frame later to get the end of the saccade
    FixOffIdx =np.array(list(compress(range(len(np.in1d(time_eye,Fixoff) )), np.in1d(time_eye,Fixoff)))) # Let them finish one frame later to get the end of the saccade

    FixOnIdx = FixOnIdx+1 # Add 1 so you get the sample after the saccade offset
    #FixOnIdx= FixOnIdx[1:len(FixOnIdx)] # Remove the first element, since this is at the beginning of the trace and will not work with the labelling
    #FixOffIdx= FixOffIdx[1:len(FixOnIdx)] # Remove the first element
    
    if FixOnIdx[-1]> FixOffIdx[-1]: # Catch an exception that only the last sample is labelled as fixation--> This will lead to problems later
        FixOnIdx = FixOnIdx[0:-2]
        FixOffIdx = FixOffIdx[0:-2]
    
    PursuitAmp = [] # Define an empty list to collect pursuit parameter
    PursuitVel = []
    PursOn = []
    PursOff = []
    
    ########################################################################################
    ## Now run through all fixation phases and check whether there is some pursuit in there
    
    for idx, on, off in zip(count(), FixOnIdx, FixOffIdx):

        # Get the onset of the current "fixation" phase
        Start = on
        StartTime = time_eye[on] 
        # Compute the a outlier velocity in this intervall that does not include a saccade (defined as percentile)

        if off-on < 1: 
            continue
        
        vel_time = vel_eye[on:off]
        Threshold = np.percentile(vel_time[Labels[on:off].astype(bool)],MinPrctile)
        
        if off + MinDuration/Samplingrate < len(vel_eye): # As long as there is the theoretical chance of finding pursuit...

            while 1: # From the start check whether you find enough samples to fullfill your criterion
            
                EndTime = time_eye[Start] # Set the end time to your start time at the beginning
                counter = 1 # Initialize a counter

                if vel_eye[Start] > Threshold or vel_eye[Start] > MinVelocity:# check whether the current velocity is above your thresholds 

                    while 1: # if yes, from the current velocity, search for how long this still is the case ... 
                        # If the velocity is above the threshold, the velocity is not nan and the search is still within the current fixation
                        if (vel_eye[Start+counter] > Threshold or vel_eye[Start+counter] > MinVelocity) and Start+counter < off:
                            counter +=1 # Then add a 1 to the counter

                        else: # if you are outside of this..
                            EndTime =  time_eye[Start+counter] # Update the endtime
                            break
                        
                ## Once you have searches through  
                if StartTime == EndTime: # If the starttime is still the endtime... Then there is no relevant sample
                    Start = Start+counter # Update the start position for the search 
                    if Start < len(time_eye): # If there is still more space to search 
                        StartTime = time_eye[Start] # update the potential start time

                else: # If you did find a relevant sample 
            
                    if EndTime-StartTime > MinDuration: # Check whether the duration is long enough to be counted as pursuit 
                    
                        # Compute some pursuit parameter and save the onset and offset 
                        PursuitAmp.append(np.sqrt((xdeg[Start+counter]-xdeg[Start])**2+(ydeg[Start+counter]-ydeg[Start])**2))
                        PursuitVel.append(np.median(vel_eye[Start:Start+counter]))
                        PursOn.append(Start)
                        PursOff.append(Start+counter)
                        
                        Start = Start+counter+1 # Update the counter to the end of the current pursuit epoch
                        if Start < len(time_eye): # If there is still more space to search 
                            StartTime = time_eye[Start] # update the potential start time

                    else: # If pursuit doesnt fulfill our criteria
                        Start = Start+counter+1 # Update the start
                        if Start+1 < len(time_eye): # If there is still more space to search 
                            StartTime = time_eye[Start] # update the potential start time

                if Start >= off: # If the start value is at the end of the fixation period.. 
                     break # .. break the search and move on to the next fixation epoch

    
    ## Now that you have everything, collect the onsets and offsets
    PursOnIdx= np.array(PursOn)
    PursOffIdx = np.array(PursOff)
    if len(PursOnIdx) > 0: # If you found pursuit responses also look up their timing        
        PursOnset = time_eye[PursOnIdx]
        PursOffset = time_eye[PursOffIdx]
    else: 
        PursOnset = np.empty(0)
        PursOffset = np.empty(0)

    return PursOnset, PursOffset,PursOnIdx,PursOffIdx # return the values
