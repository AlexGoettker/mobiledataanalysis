## Define Fixations as the rest categorie and save their parameter 
import numpy as np 
import pandas as pd 
from itertools import count
from itertools import compress
import os 
import AnalysisTools as at

def compute_parameter_fix(xpos,ypos,xdeg,ydeg,time_eye,PursOnIdx,PursOffIdx,UpdtSacconIdx,UpdtSaccoffIdx,VOROnIdx,VOROffIdx,Blink_Vek,Datapath,Rec,MinFix):
    
    Labels = np.ones(len(time_eye))    #Get a label vector to mark  on and offsets of fixations 
    for idx, on, off in zip(count(), UpdtSacconIdx, UpdtSaccoffIdx): # Add the saccades
        Labels[on:off] = 0

    for idx, on, off in zip(count(), PursOnIdx, PursOffIdx): # Add the Pursuit
        Labels[on:off] = 0
    
    for idx, on, off in zip(count(), VOROnIdx, VOROffIdx): # Add the VOR
        Labels[on:off] = 0

    Labels[Blink_Vek] =0 # Add the blinks
   
    [Fixon, Fixoff] = at.FindSaccs(Labels,time_eye) 

    FixOnIdx = np.array(list(compress(range(len(np.in1d(time_eye,Fixon) )), np.in1d(time_eye,Fixon))))# Let them start one frame later to get the end of the saccade
    FixOffIdx =np.array(list(compress(range(len(np.in1d(time_eye,Fixoff) )), np.in1d(time_eye,Fixoff)))) # Let them finish one frame later to get the end of the saccade

    Fixon[0] = 0 # Update this to get the initial fixation from the beginning already
    FixOnIdx[0]=0

    #Initialize things
    FixMat = np.zeros((len(FixOnIdx),9))
    ValidFix = []

    for idx, on, off in zip(count(), FixOnIdx, FixOffIdx):

        if Fixoff[idx]-Fixon[idx] > MinFix: # If the Fixation has the minimum fixation duration 
            
            # Get the characteristics
            FixMat[idx,0] = on # Onset in frames 
            FixMat[idx,1] = off # Offset in frames 
            FixMat[idx,2] = Fixon[idx] # Onset in time 
            FixMat[idx,3] = Fixoff[idx] # Offset in time 
            FixMat[idx,4] = Fixoff[idx]-Fixon[idx] # Duration of fixation [ms] 
            FixMat[idx,5] = np.median(xpos[on:off]) # x-Position in Pixel 
            FixMat[idx,6] = np.median(ypos[on:off]) # y-Position in Pixel 
            FixMat[idx,7] = np.median(xdeg[on:off]) # x-Position in deg 
            FixMat[idx,8] = np.median(ydeg[on:off]) # y-Position in deg

            # if np.isnan(np.median(xpos[on:off])):
            #     print(on,off,xpos[on:off])

            
            ValidFix.append(True)
        else: 
            ValidFix.append(False)

    FixMat = FixMat[ValidFix]

    FixOn = FixMat[:,2]
    FixOff =FixMat[:,3]

    df = pd.DataFrame(FixMat,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','Fixation Duration [ms]','X-Position [pix]','Y-Position [pix]','X-Position [deg]','Y-Position [deg]'])
    df.to_csv(os.path.join(Datapath,Rec,'FixParam.csv'), index=False)

    return FixOn, FixOff

    