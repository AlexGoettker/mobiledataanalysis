""" The function to save the pursuit parameter """
import numpy as np 
import pandas as pd 
from itertools import count
import os 
def compute_parameter_purs(xdeg,ydeg,vel_eye,PursOn,PursOff,PursOnIdx,PursOffIdx,Datapath,Rec):
    
    ## This function takes the pursuit labels and produces an output file where all relevant parameters are saved 
    # Input args: 
        # x-position
        # y-position 
        # eye velocity 
        # Pursuit Onsets as Idx
        # Pursuit Offsets as Idx
        # Pursuit Onsets as time
        # Pursuit Offset as time 
    
    # Output args 
        # Panda with pursuit parameter:
            # On and offset in frames 
            # On and offset in time 
            # Pursuit Duration 
            # Pursuit Amplitude
            # Pursuit median Vel
            # Pursuit Direction
    
    PursParam = np.zeros((len(PursOn),8)) # Initialize the saving array

    for counter, on, off in zip(count(), PursOnIdx, PursOffIdx): # Loop through all of the saccades 
        PursParam[counter,0] = on # Onset in frames
        PursParam[counter,1] = off # Offset in frames
        PursParam[counter,2] = PursOn[counter] # Offset in time
        PursParam[counter,3] = PursOff[counter] # Onset in time
        PursParam[counter,4] = PursOff[counter]-PursOn[counter] # Pursuit Duration [ms]
        PursParam[counter,5] = np.sqrt((xdeg[off]-xdeg[on])**2+(ydeg[off]-ydeg[on])**2)  # Pursuit Amplitude [deg]
        PursParam[counter,6] = np.median(vel_eye[on:off])  # Pursuit mean velocity [deg]
        PursParam[counter,7] = np.rad2deg(np.arctan2(ydeg[off]-ydeg[on], xdeg[off]-xdeg[on])) # Pursuit Direction [°]


    df = pd.DataFrame(PursParam,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','Pursuit Duration [ms]','Pursuit Amplitude [deg]','Pursuit MeanVel [deg/s]','Pursuit Direction [°]'])
    df.to_csv(os.path.join(Datapath,Rec,'PursParam.csv'), index=False)
  