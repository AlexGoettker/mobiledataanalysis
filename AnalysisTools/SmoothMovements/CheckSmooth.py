import numpy as np
from itertools import count

def CheckSmooth(PursOn,PursOff,PursOnIdx,PursOffIdx,UpdtSacconIdx,MinFix):
    
    ## This function checks whether there is enough time between identified pursuit epochs
    ## if not --> It merges them to one epoch 

    # Input args: 
        # Pursuit onset time
        # Pursuit offset time
        # Pursuit onset index
        # Pursuit offset index
        # Saccade Onset index
        # Saccade Offset index
        
    # Output args 
        # Updated Pursuit on and offset in time 
        # Updated Pursuit on and offset as index 

    # Intialize some variables 
    ValidPurs = []
    Skip = 0
    for counter, on, off in zip(count(), PursOn, PursOff): # Loop through all of the smooth movements 
        
        if counter < len(PursOff)-1: # Until the last one 

            if Skip: # if you merged this one to the previous one, then assign this one a false
                ValidPurs.append(False)
                Skip = 0
                continue
            else: 
                # Compute the time between the smooth phases
                Time_Between = PursOn[counter+1]-PursOff[counter] 
                Sacc_Between = np.any(np.array(np.logical_and(np.greater(UpdtSacconIdx,PursOffIdx[counter]), np.less(UpdtSacconIdx,PursOnIdx[counter+1])))) # Check whether there is a saccade in between

                if Time_Between > MinFix or Sacc_Between: # If the time is large enough or there is a saccade, smoot movement in valid
                    ValidPurs.append(True)
                else: # If not... 
                    PursOff[counter] = PursOff[counter+1] # Change the end to the end of the following response 
                    PursOffIdx[counter] = PursOffIdx[counter+1]
                    ValidPurs.append(True) # Then label it as valid
                    Skip = 1   # Remember that this means you have delt with the next one
        else: 
            ValidPurs.append(True)

    # At the end, update the lists with the valid pursuit responses
    PursOn = PursOn[ValidPurs]
    PursOff = PursOff[ValidPurs]
    PursOnIdx = PursOnIdx[ValidPurs]
    PursOffIdx = PursOffIdx[ValidPurs]

    return PursOn,PursOff,PursOnIdx,PursOffIdx

