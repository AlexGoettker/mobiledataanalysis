""" The function to save the VOR parameter """
import numpy as np 
import pandas as pd 
from itertools import count
import os 
def compute_parameter_vor(xdeg,ydeg,vel_eye,vx_head,vy_head,VOROn,VOROff,VOROnIdx,VOROffIdx,time_head,time_eye,Datapath,Rec,Samplingrate):
    
    ## This function takes the saccade labels and produces an output file where all relevant parameters are saved 
    # Input args: 
        # x-position
        # y-position 
        # eye velocity 
        # Vor Onsets as Idx
        # Vor Offsets as Idx
        # Vor Onsets as time
        # Vor Offsets as time 
    
    # Output args 
        # Panda with pursuit parameter:
            # On and offset in frames 
            # On and offset in time 
            # VOR Duration 
            # VOR Eye Amplitude
            # VOR Eye Median Vel 
            # VOR Eye Direction Eye
            # VOR Head Amplitude
            # VOR Head Median Vel 
            # VOR Head Direction
            # VOR Amplitude Gain 
            # VOR Velocity Gain
    
    VORParam = np.zeros((len(VOROn),13)) # Initialize the saving array

    for counter, on, off in zip(count(), VOROnIdx, VOROffIdx): # Loop through all of the saccades 
        VORParam[counter,0] = on # Onset in frames
        VORParam[counter,1] = off # Offset in frames
        VORParam[counter,2] = VOROn[counter] # Offset in time
        VORParam[counter,3] = VOROff[counter] # Onset in time
        VORParam[counter,4] = VOROff[counter]-VOROn[counter] # VOR Duration [ms]

        # For the eye movement
        VORParam[counter,5] = np.sqrt((xdeg[off]-xdeg[on])**2+(ydeg[off]-ydeg[on])**2)  # VOR Amplitude [deg]
        VORParam[counter,6] = np.median(vel_eye[on:off])  # VOR mean velocity [deg]
        VORParam[counter,7] = np.rad2deg(np.arctan2(ydeg[off]-ydeg[on], xdeg[off]-xdeg[on])) # VOR Direction [°]

        # For the head movement
        idx = np.array(np.logical_and(np.greater(time_head,time_eye[on]), np.less(time_head,time_eye[off]))) # Find the matching head movement sampless

         # Compute the head x and y velocity 
        HMean_Vel_x = np.median(vx_head[idx])
        HMean_Vel_y = np.median(vy_head[idx])

        # Compute the distance the head travelled in x,y and total 
        HDist_x = np.sum(vx_head[idx]/Samplingrate)
        HDist_y = np.sum(vy_head[idx]/Samplingrate)

        # Add the head direction things in the array
        VORParam[counter,8] = np.sqrt(HDist_x**2+HDist_y**2) # VOR Amplitude [deg]
        VORParam[counter,9] = np.sqrt(HMean_Vel_x**2+HMean_Vel_y**2)  # VOR Head Velocity [deg/s]
        VORParam[counter,10] = np.rad2deg(np.arctan2(HDist_y, HDist_x)) # VOR Direction [°]

        # Compute the gain 
        VORParam[counter,11] =  VORParam[counter,5] / VORParam[counter,8]
        VORParam[counter,12] =  VORParam[counter,6] / VORParam[counter,9]


    df = pd.DataFrame(VORParam,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','VOR Duration [ms]','VOR EYE Amplitude [deg]','VOR EYE MeanVel [deg/s]','VOR EYE Direction [°]','VOR HEAD Amplitude [deg]','VOR HEAD MeanVel [deg/s]','VOR HEAD Direction [°]','VOR Distance Gain', 'VOR Velocity Gain'])
    df.to_csv(os.path.join(Datapath,Rec,'VORParam.csv'), index=False)
  