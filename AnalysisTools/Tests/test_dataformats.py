import cv2

def test_dataformats(Eye,Head,Video,cap):
    assert Eye.shape[1] == 10, "Gaze Data doesnt have the right shape"
    assert Head.shape[1] == 16, "Head Data doesnt have the right shape"
    assert int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) == Video.shape[0], "Frames of Video do not match the number of timestamps" 



