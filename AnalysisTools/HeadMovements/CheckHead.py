import numpy as np 
from itertools import count


def CheckHead(PursOn,PursOff,PursOnIdx,PursOffIdx,MinFix):
    ## This function checks whether there is enough time between head movements --> If not merge them 
    # Same as for pursuit, just saccade criterion is gone 
     # Input args: 
        # Pursuit onset time
        # Pursuit offset time
        # Pursuit onset index
        # Pursuit offset index
          
    # Output args 
        # Updated Pursuit on and offset in time 
        # Updated Pursuit on and offset as index 

    # Initialize things
    ValidPurs = []
    Skip = 0
    
    for counter, on, off in zip(count(), PursOn, PursOff): # Loop through all of the head moements 
        
        if counter < len(PursOff)-1:

            if Skip: # If you skipped the this one before... 
                ValidPurs.append(False) # Label it as wrong 
                Skip = 0
                continue
            else: 
                # Compute the time between the head movement phases
                Time_Between = PursOn[counter+1]-PursOff[counter]

                if Time_Between > MinFix: # if there is enough time
                    ValidPurs.append(True) # Label it as valid
                else: # If not..
                    PursOff[counter] = PursOff[counter+1] # Update the end as the end of the next one 
                    PursOffIdx[counter] = PursOffIdx[counter+1]
                    ValidPurs.append(True)
                    Skip = 1   
        else: 
            ValidPurs.append(True)

    # When you have all labels, update the variables 
    PursOn = PursOn[ValidPurs]
    PursOff = PursOff[ValidPurs]
    PursOnIdx = PursOnIdx[ValidPurs]
    PursOffIdx = PursOffIdx[ValidPurs]

    return PursOn,PursOff,PursOnIdx,PursOffIdx
