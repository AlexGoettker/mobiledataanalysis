"""Main Function for pursuit labelling """
import numpy as np

def FindHead(vel_head,time_head,Windowlength,MinPrctile,MinVel,MinDur):
     ## This function finds head movement epochs 
    # Input args: 
        # head velocity
        # time vector of head movements
        
    # Output args 
        # Head movement on and offset in time 
        # Head movement on and offset as index 

    on = 0 
    off = len(vel_head)    
    HeadOn = []
    HeadOff =[]  
    ########################################################################################
    ## Now run through the whole recording check whether there are head movements in there
    Start = on
    StartTime = time_head[on] 
    while 1:
        # Compute the a outlier velocity in this intervall that does not include a saccade (defined as percentile)
        vel_time = vel_head[Start:Start+Windowlength]
        Threshold = np.percentile(vel_time,MinPrctile) # Get the percentile threshold in the current window

        while 1: # From the start check whether you find enough samples to fullfill your criterion
            
            EndTime = time_head[Start] # Set the end time to your start time at the beginning
            counter = 1 # Initialize a counter
            #print(counter,off)
            if vel_head[Start] > Threshold or vel_head[Start] > MinVel:# check whether the current velocity is above your thresholds 

                while 1: # if yes, from the current velocity, search for how long this still is the case ... 
                    # If the velocity is above the threshold, the velocity is not nan and the search is still within the current fixation
                    if Start+counter <= off-1:
                        if (vel_head[Start+counter] > Threshold or vel_head[Start+counter] > MinVel):
                            counter +=1 # Then add a 1 to the counter

                        else: # if you are outside of this..
                            EndTime =  time_head[Start+counter] # Update the endtime
                            break
                    else:
                        counter = counter-1
                        EndTime =  time_head[len(time_head)-1] # Update the endtime
                        break 
            #print(StartTime,EndTime)            
            ## Once you searched
            if StartTime == EndTime: # If the starttime is still the endtime... Then there is no relevant sample
                #print('Same Start and End')
                Start = Start+counter # Update the start position for the search 
                if Start < len(time_head): # If there is still more space to search 
                    StartTime = time_head[Start] # update the potential start time

            else: # If you did find a relevant sample 
                if EndTime-StartTime > MinDur: # Check whether the duration is long enough to be counted as pursuit 
                    #  save the onset and offset       
                    HeadOn.append(Start)
                    HeadOff.append(Start+counter)
                        
                    Start = Start+counter+1 # Update the counter to the end of the current pursuit epoch
                    if Start < len(time_head): # If there is still more space to search 
                        StartTime = time_head[Start] # update the potential start time

                else: # If pursuit doesnt fulfill our criteria
                    Start = Start+counter+1 # Update the start
                    if Start+1 < len(time_head): # If there is still more space to search 
                        StartTime = time_head[Start] # update the potential start time
            
            if Start >= off-1: # If the start value is at the end of the fixation period.. 
                break # .. break the search and move on to the next fixation epoch
        break

    
    ## Now that you have everything, collect the onsets and offsets
    HeadOnIdx= np.array(HeadOn)
    HeadOffIdx = np.array(HeadOff)
    HeadOnset = time_head[HeadOnIdx]
    HeadOffset = time_head[HeadOffIdx]
    return HeadOnset, HeadOffset,HeadOnIdx,HeadOffIdx

