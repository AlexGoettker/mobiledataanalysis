""" The function to save the Head movement parameter """
import numpy as np 
import pandas as pd 
from itertools import count
import os 

def compute_parameter_head(vx_head,vy_head,HeadOn,HeadOff,HeadOnIdx,HeadOffIdx,Datapath,Rec,SamplingRate):
    
    ## This function takes the pursuit labels and produces an output file where all relevant parameters are saved 
    # Input args: 
        # x-velocity of head
        # y-velocity of head
        # Head Onsets as Idx
        # Head Offsets as Idx
        # Head Onsets as time
        # Head Offset as time 
    
    # Output args 
        # Panda with Head parameter:
            # On and offset in frames 
            # On and offset in time 
            # Head Duration 
            # Head Amplitude
            # Head median Vel
            # Head Direction
    
    HeadParam = np.zeros((len(HeadOn),8)) # Initialize the saving array

    for counter, on, off in zip(count(), HeadOnIdx, HeadOffIdx): # Loop through all of the saccades 
        HeadParam[counter,0] = on # Onset in frames
        HeadParam[counter,1] = off # Offset in frames
        HeadParam[counter,2] = HeadOn[counter] # Offset in time
        HeadParam[counter,3] = HeadOff[counter] # Onset in time
        HeadParam[counter,4] = HeadOff[counter]-HeadOn[counter] # Pursuit Duration [ms]
        
        # Compute the head x and y velocity 
        HMean_Vel_x = np.median(vx_head[on:off])
        HMean_Vel_y = np.median(vy_head[on:off])

        # Compute the distance the head travelled in x,y and total 
        HDist_x = np.sum(vx_head[on:off]/SamplingRate)
        HDist_y = np.sum(vy_head[on:off]/SamplingRate)

        HeadParam[counter,5] = np.sqrt(HDist_x**2+HDist_y**2) # Head Amplitude [deg]
        HeadParam[counter,6] = np.sqrt(HMean_Vel_x**2+HMean_Vel_y**2)  # Head Head Velocity [deg/s]
        HeadParam[counter,7] = np.rad2deg(np.arctan2(HDist_y, HDist_x)) # Head Direction [°]

      
    df = pd.DataFrame(HeadParam,columns = ['Onset [frame]', 'Offset [frame]', 'Onset [ms]','Offset [ms]','Head Duration [ms]','Head Amplitude [deg]','Head MeanVel [deg/s]','Head Direction [°]'])
    df.to_csv(os.path.join(Datapath,Rec,'HeadParam.csv'), index=False)
  