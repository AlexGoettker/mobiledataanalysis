import numpy as np 
from itertools import count

def CheckVOR(time_eye,PursOnIdx,PursOffIdx,vel_x,vel_y,time_head,vx_head,vy_head,Samplingeye,Samplinghead,DirectionWindow,DistanceWindow):
    ## This function distingushes the smooth eye movement labels into pursuit and VOR/Fixations 
     # Input args: 
        # time_eye in ms
        # Pursuit onset index
        # Pursuit offset index
        # velocity eye x 
        # velocity eye y
        # time vector of the head movements
        # X-velocity of head movement
        # Y-velocity of head movement
          
    # Output args 
        # List of VOR movement
        # List of Pursuit movements
    
    # Initialize the lists 
    VOR = []
    Pursuit = []

    for counter, on, off in zip(count(), PursOnIdx, PursOffIdx): # Loop through all of the smooth phases 
        
        ########################## Eye movements ####################################                  
        # Compute the eye velocity in x and y          
        Mean_Vel_x = np.median(vel_x[on:off]) 
        Mean_Vel_y = np.median(vel_y[on:off])
        
        # Compute the distance the eye travelled in x,y and total 
        Dist_x = np.sum(vel_x[on:off]/Samplingeye)
        Dist_y = np.sum(vel_y[on:off]/Samplingeye)
        Dist_t = np.sqrt(Dist_x**2+Dist_y**2)

        # Compute the eye direction in the smooth phase
        Dir_Eye = np.rad2deg(np.arctan2(Dist_y, Dist_x))

        ########################## Head movements ####################################                  
        idx = np.array(np.logical_and(np.greater(time_head,time_eye[on]), np.less(time_head,time_eye[off]))) # Find the matching head movement sampless

        # Compute the head x and y velocity 
        HMean_Vel_x = np.median(vx_head[idx])
        HMean_Vel_y = np.median(vy_head[idx])

        # Compute the distance the head travelled in x,y and total 
        HDist_x = np.sum(vx_head[idx]/Samplinghead)
        HDist_y = np.sum(vy_head[idx]/Samplinghead)
        HDist_t = np.sqrt(HDist_x**2+HDist_y**2)

        # Compute the head direction in the smooth phase
        Dir_Head = np.rad2deg(np.arctan2(HDist_y, HDist_x))

        ################ Now compare eye and head #######################

        # If eye and head move in opposite directions and cover a roughly similar distance 
        if np.abs(Dir_Eye-Dir_Head) > 180-DirectionWindow and np.abs(Dir_Eye-Dir_Head) < 180+DirectionWindow and Dist_t/HDist_t > 1-DistanceWindow and  Dist_t/HDist_t < 1+DistanceWindow: 
            VOR.append(True) # It is a VOR ...
            Pursuit.append(False) # ... and not a pursuit response
        else: # If they dont fulfill the criteria above
            VOR.append(False) # It is not a VOR ..
            Pursuit.append(True) # ... but a pursuit response

     # Output the lists 
    return VOR, Pursuit

