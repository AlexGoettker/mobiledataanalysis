import cv2
import os 
import numpy as np
import torch 
import glob
from unidepth.models import UniDepthV1
from PIL import Image
import matplotlib.pyplot as plt


def DepthLabelling(Start,End,time_video,time_eye,xpos,ypos,Datapath,Rec):
  ## This function creates a video of the world camera with gaze overlayed
    # Input args: 
        # Datapath of the folder containing the recordings
        # Resultpath where you want to save the video 
        # Name of the recording
        # Specify in which frame you want to start the recording --> Needs index not time
        # Specify in which frame you want to end the recording --> Needs index not time
        # xpos eye in pixel
        # ypos eye in pixel 
        # time vector the eye samples 
        # time vector of the video frames 
    
    # Output args 
        # None
    # Video file will be saved in the provided folder 
    ##########################################################################
    
    # Start the model 
    
    # Get the basic instantiation
    model = UniDepthV1.from_pretrained("lpiccinelli/unidepth-v1-vitl14") # or "lpiccinelli/unidepth-v1-cnvnxtl" for the ConvNext backbone

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)

    FixDepth = np.zeros(((End-Start)+1,1))

    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    counter =0 # Init frame counter
    frame_counter = 0

    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 
    while cap.isOpened():
        ret, frame = cap.read()# run through the window...
    
        if counter >= Start and counter <= End: # get only the relevant frames
            #print('Analyzing frame')
            # Turn the image into a depthmap 
            img = frame[:, :, ::-1]
           
            rgb = torch.from_numpy(img.copy()).permute(2, 0, 1) # C, H, W
            predictions = model.infer(rgb)
            depth = predictions["depth"] # Metric Depth Estimation
            depthmap = torch.Tensor.cpu(depth).numpy() # get the depthmap 
            depthmap = depthmap[0,0,:,:] 

            TimeFrameVideo = [time_video[counter]-15, time_video[counter]+15] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate
            
            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window
            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])
           # print(TimeFrameVideo,idx, X,Y)

            if ~np.isnan(X) and int(Y) > 0 and int(Y) < np.size(depthmap,0) and int(X) > 0 and int(X) < np.size(depthmap,1) : 
                FixDepth[frame_counter]= depthmap[int(Y),int(X)]
            else: 
                FixDepth[frame_counter] = np.nan
            
            print('You have ', frame_counter+1, 'frames out of', (End-Start)+1)
            frame_counter+=1


        # Show and update the counter
        counter +=1

        # If you readed the end of the relevant frames you can stop
        if counter > End:
            break
    
    return FixDepth
   