"""Main Function for Scene Labelling """

import os 
import glob
import cv2
import numpy as np
import matplotlib.pyplot as plt

# Import Detectron Things 
import torch
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger() # Run the setup logger 
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog

def SceneLabelling(Start,End,time_video,time_eye,xpos,ypos,Datapath,Rec,SystemName,Radius): 
     ## This function the inpute image and gaze position and looks at the distribution of 
     ## available objects in the scene as well as which are fixated
    # Input args: 
        # Start frame for the analysis
        # End frame for the analysis
        # time of the video
        # time of the eye data
    
    # Output args 
        # Fixation Matrix that contains a row for each frame and columns for each possible object
        # --> If it is one, then in the current frame, this category was fixated 
        # Object Matrix that contains a row for each frame and columns for each possible object
        # --> It contains the number of pixel that belonged to each object that was labled in a frame 
    # Please note here that there is a rest category which is not in the Labelled list (Last object in each Matrix, index 133), 
    # where the things are saved that were not succesfully labelled 
        
    # Load the incoming video 
    vidname = glob.glob(os.path.join(Datapath,Rec,'*.mp4'))
    cap = cv2.VideoCapture(vidname[0]) # World Video

    # Define the which objects are still considered to be fixated
    counter =0
    # Use while to read in the frames, ineffecient for late time points, but works reliably, not like gettin individual frames 

    MatFix = np.zeros(((End-Start)+1,134))
    MatObj = np.zeros(((End-Start)+1,134))

    frame_counter = 0
    
    while cap.isOpened():
        ret, frame = cap.read()# run through the window..

        if counter >= Start and counter <= End: # get only the relevant frames
            frame = frame[:, :, ::-1]# get them into rgb

            ################### Gaze ###################################
            TimeFrameVideo = [time_video[counter-1]-15, time_video[counter-1]+15] # Get the timeframe you want for gaze +/- 15 ms, this roughly matches the 30Hz sampling rate

            idx = np.array(np.logical_and(np.greater(time_eye,TimeFrameVideo[0]), np.less(time_eye,TimeFrameVideo[1]))) # Find the gaze samples that are in the time window

            X = np.median(xpos[idx]) # Compute the gaze position 
            Y = np.median(ypos[idx])
            # Now create a distance map based on gaze 
            X_Vec = np.arange(1,round(frame.shape[1]+1))-X
            Y_Vec = np.arange(1,round(frame.shape[0]+1))-Y
            x,y = np.meshgrid(Y_Vec,X_Vec)
            Dist = np.sqrt(x**2+y**2)
                       
            ################# Image Processing ############################

            plt.imshow(frame) # Show the image 

            # Inference with a panoptic segmentation model
            cfg = get_cfg() # Get the image to Detectron

            ############
            if SystemName != 'linux':
                cfg.MODEL.DEVICE = 'cpu' # This is here to make it run on mac, not needed when running it with regular gpu
            ############

            #Load the detault weigths
            cfg.merge_from_file(model_zoo.get_config_file("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")) 
            cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")

            # Predict the labels 
            predictor = DefaultPredictor(cfg)
            panoptic_seg, segments_info = predictor(frame)["panoptic_seg"]

            # Now get at the labels 
            LabelPositions = torch.Tensor.cpu(panoptic_seg).numpy().astype('int') # save the pixel masks

            # Get the object labels 
            Objects = np.arange(0,len(segments_info),1)
            Mat = np.zeros((len(segments_info),4))

            for obj in Objects: 
                
                Mat[obj,0] = segments_info[obj].get("id")
                Mat[obj,1] = segments_info[obj].get("score")
                Mat[obj,2] = segments_info[obj].get("category_id")
                Mat[obj,3] = segments_info[obj].get("instance_id")

                # Get the respective category
                if np.isnan(Mat[obj,1]):
                    FixCat = int(Mat[obj,2]+79) # This is the belonging entry in the category list 
                else:
                    FixCat = int(Mat[obj,2]) # This is the belonging category in the list 

                MatObj[frame_counter,FixCat] = np.count_nonzero(LabelPositions == obj) # Count the number of pixels for each object 


            MatObj[frame_counter,133] =  np.count_nonzero(LabelPositions == 0) # Count the unlabelled pixels

            ###### Look at which objects are fixated 
            fixpix = np.array(np.where((Dist.T < Radius)))
            FixatedObjects = np.unique(LabelPositions[fixpix[0],fixpix[1]])-1 # To match the indexing 

            FixCat = np.zeros(len(FixatedObjects))
            for idx, obj in enumerate(FixatedObjects):
                if np.isnan(Mat[obj,1]):
                    FixCat[idx] = int(Mat[obj,2]+79) # This is the belonging entry in the category list 
                elif FixatedObjects[idx] < 0: 
                    FixCat[idx] = 133 # Additional category to mark the ones that were not fixated
                else:
                    FixCat[idx] = int(Mat[obj,2]) # This is the belonging category in the list 
            
            SelectObject = (np.unique(np.round(FixCat))).astype(int)
            MatFix[frame_counter,SelectObject] = 1 # Mark the objects that are fixated
            
            print('Analyzed Frame:',frame_counter+1, 'out of', (End-Start)+1)
            frame_counter  +=1 # Update the frame counter          

        counter += 1 # move on to the next frame  

        if counter > End:
            return MatFix,MatObj