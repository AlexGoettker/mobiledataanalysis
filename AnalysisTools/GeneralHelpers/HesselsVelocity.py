import numpy as np

def HesselsVelocity(pos,time): # Define the velocity in the same way as in the original Hessels article
    
    dt1	= time[1:-1] - time[0:-2] # Compute a shifted difference in time and position 
    dx1	= pos[1:-1] - pos[0:-2]

    dt2	= time[2:len(time)] - time[1:-1]
    dx2	= pos[2:len(time)] - pos[1:-1]

    vel_comp = (dx1/dt1 + dx2/dt2)/2 # Get the velocity 

    # The first and last value are missing in this way, replace them with Nans
    vel = np.nan
    vel = np.append(vel, vel_comp)
    vel = np.append(vel,np.nan)

    return vel



