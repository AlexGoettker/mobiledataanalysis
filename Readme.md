This will a pipeline to analyze mobile eye tracking data. 
Desired Features will be: 

    Saccade Detection 
    Smooth Eye Movements (Pursuit & VOR) detection
    Head Movement Detection 
    Automatic Labeling of the Scene Camera based on computer vision 

Implemented: Saccade Detection, Smooth Eye movement and Head movement Detection. Includes a first try at differentiating Pursuit and VOR based on head movements.  

Automatic Labeling with Detectron 2 is implemented for scene labelling. 

Includes demos and visualizations for the data.

contact: alexander.goettker@psychol.uni-giessen.de
