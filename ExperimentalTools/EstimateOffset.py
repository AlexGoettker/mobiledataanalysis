## Try using the pupil labs real-time API to achieve synchronization between your computer and the neon glasses 
# This is explained and documented here: https://docs.pupil-labs.com/neon/data-collection/time-synchronization/


from pupil_labs.realtime_api.simple import discover_one_device
from pupil_labs.realtime_api.simple import Device

# Look for devices. Returns as soon as it has found the first device.
print("Looking for the next best device...")
#device = discover_one_device(max_search_duration_seconds=10)
#if device is None:
#    raise SystemExit("No device found.")

# This address is just an example. Find out the actual IP address of your device!
ip = "134.176.109.162"
device = Device(address=ip, port="8080")

print(f"Phone IP address: {device.phone_ip}")
print(f"Phone name: {device.phone_name}")
print(f"Battery level: {device.battery_level_percent}%")
print(f"Free storage: {device.memory_num_free_bytes / 1024**3:.1f} GB")
print(f"Serial number of connected glasses: {device.serial_number_glasses}")

estimate = device.estimate_time_offset()
if estimate is None:
    device.close()
    raise SystemExit("Neon Companion app is too old")

print(f"Mean time offset: {estimate.time_offset_ms.mean} ms")
print(f"Mean roundtrip duration: {estimate.roundtrip_duration_ms.mean} ms")

device.close()